---
wsId: crypterApp
title: The Crypto App - Coin Tracker
altTitle: 
authors:
- danny
appId: com.CryptoCurrency.dev
appCountry: us
idd: 1339112917
released: 2018-02-21
updated: 2024-04-18
version: 3.3.8
stars: 4.7
reviews: 2370
size: '225088512'
website: https://thecrypto.app
repository: 
issue: 
icon: com.CryptoCurrency.dev.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-01
signer: 
reviewArchive: 
twitter: TrustSwap
social:
- https://www.linkedin.com/company/TrustSwap
- https://www.facebook.com/TrustSwap
features: 
developerName: Trustswap Inc.

---

{% include copyFromAndroid.html %}
