---
wsId: stablexKripto
title: Stablex | Kripto Para Al-Sat
altTitle: 
authors:
- danny
appId: net.stablex.app
appCountry: tr
idd: '1591457284'
released: 2021-10-26
updated: 2024-05-16
version: 2.5.10
stars: 3.5
reviews: 38
size: '51220480'
website: https://stablex.com.tr
repository: 
issue: 
icon: net.stablex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-03
signer: 
reviewArchive: 
twitter: Stablex_Turkey
social:
- https://www.instagram.com/stablexofficial
- https://www.youtube.com/channel/UC261DPjgdgZcP9PgoKrGc7A
- https://www.linkedin.com/company/stablex-net
features: 
developerName: STABLEX BILISIM TEKNOLOJI ANONIM SIRKETI

---

{% include copyFromAndroid.html %}
