---
wsId: tapbitCrypto
title: Tapbit
altTitle: 
authors:
- danny
appId: com.billance.cn
appCountry: us
idd: '1610497530'
released: 2022-03-01
updated: 2024-05-17
version: 3.6.3
stars: 4.4
reviews: 16
size: '72223744'
website: 
repository: 
issue: 
icon: com.billance.cn.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: tapbitglobal
social:
- https://www.tapbit.com
- https://www.linkedin.com/company/tapbit
- https://www.facebook.com/Tapbitglobal
- https://www.reddit.com/user/tapbit
- https://www.youtube.com/c/Tapbitglobal
features: 
developerName: Tapbit LLC

---

{% include copyFromAndroid.html %}
