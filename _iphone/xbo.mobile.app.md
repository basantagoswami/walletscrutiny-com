---
wsId: xboCrypto
title: XBO.com - Buy Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: xbo.mobile.app
appCountry: cy
idd: '1638748643'
released: 2022-09-24
updated: 2024-05-15
version: 2.0.13
stars: 5
reviews: 5
size: '141928448'
website: https://www.xbo.com/
repository: 
issue: 
icon: xbo.mobile.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-25
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: XBO

---

{% include copyFromAndroid.html %}