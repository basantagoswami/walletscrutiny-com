---
wsId: Baksman
title: Baksman – buy Bitcoin
altTitle: 
authors:
- danny
appId: com.xchange.baksman
appCountry: ru
idd: 1436169013
released: 2018-09-21
updated: 2023-08-28
version: 2.2.9
stars: 4.7
reviews: 63
size: '14303232'
website: 
repository: 
issue: 
icon: com.xchange.baksman.jpg
bugbounty: 
meta: removed
verdict: nowallet
date: 2024-05-07
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/baksmancom
features: 
developerName: XChangeCapitalGroup

---

{% include copyFromAndroid.html %}
