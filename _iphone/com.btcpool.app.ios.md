---
wsId: BTCcomPool
title: BTC.com - Leading Mining Pool
altTitle: 
authors:
- danny
appId: com.btcpool.app.ios
appCountry: us
idd: 1490997527
released: 2020-01-21
updated: 2023-08-31
version: 2.3.3
stars: 3.5
reviews: 12
size: '63212544'
website: https://btc.com/
repository: 
issue: 
icon: com.btcpool.app.ios.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-10
signer: 
reviewArchive: 
twitter: btccom_official
social:
- https://www.linkedin.com/company/btc.com
- https://www.facebook.com/btccom
features: 
developerName: Beijing GuiXinYangHang Technology Co., Ltd.

---

{% include copyFromAndroid.html %}
