---
wsId: jgCapital
title: J G Capital
altTitle: 
authors:
- danny
appId: com.suffescom.jeangilles
appCountry: us
idd: '1642934824'
released: 2022-09-16
updated: 2024-03-22
version: 1.3.0
stars: 4
reviews: 9
size: '16142336'
website: 
repository: 
issue: 
icon: com.suffescom.jeangilles.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-18
signer: 
reviewArchive: 
twitter: 
social:
- https://jgcapitalbitcoin.com
- https://www.facebook.com/jeangillescapital
features: 
developerName: Jean Gilles Capital LLC

---

{% include copyFromAndroid.html %}