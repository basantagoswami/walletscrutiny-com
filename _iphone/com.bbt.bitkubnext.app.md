---
wsId: bitkubNext
title: Bitkub NEXT
altTitle: 
authors:
- danny
appId: com.bbt.bitkubnext.app
appCountry: th
idd: '6444399387'
released: 2022-12-07
updated: 2024-05-07
version: 2.3.0
stars: 4.9
reviews: 235
size: '121333760'
website: https://www.bitkubchain.com
repository: 
issue: 
icon: com.bbt.bitkubnext.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-03-29
signer: 
reviewArchive: 
twitter: bitkubchain
social:
- https://www.facebook.com/bitkubchainofficial
features: 
developerName: Bitkub Blockchain Technology Co., Ltd.

---

{% include copyFromAndroid.html %}
