---
wsId: coinme
title: 'Coinme: Buy Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.coinme.CoinMe
appCountry: us
idd: 1545440300
released: 2021-05-11
updated: 2024-05-23
version: 2.2.23
stars: 4.6
reviews: 4732
size: '130856960'
website: https://coinme.com/
repository: 
issue: 
icon: com.coinme.CoinMe.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: Coinme
social:
- https://www.linkedin.com/company/coinme
- https://www.facebook.com/Coinme
features: 
developerName: Coinme Inc.

---

 {% include copyFromAndroid.html %}
