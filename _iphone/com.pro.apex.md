---
wsId: apexProTrade
title: 'ApeX Protocol: Trade Crypto'
altTitle: 
authors:
- danny
appId: com.pro.apex
appCountry: us
idd: '1645456064'
released: 2022-09-27
updated: 2024-05-23
version: 1.28.5
stars: 4.2
reviews: 51
size: '85326848'
website: 
repository: 
issue: 
icon: com.pro.apex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-02
signer: 
reviewArchive: 
twitter: OfficialApeXdex
social:
- https://apex.exchange
- https://apexdex.medium.com
- https://discord.com/invite/366Puqavwx
- https://t.me/ApeXdex
features: 
developerName: APEX DAO LLC

---

{% include copyFromAndroid.html %}
