---
wsId: coinInnBTCandDoge
title: CoinInn:Buy BTC and Doge
altTitle: 
authors:
- danny
appId: com.coininn.yykik
appCountry: us
idd: '1501704472'
released: 2020-03-08
updated: 2023-08-22
version: 3.0.38
stars: 4.1
reviews: 44
size: '167042048'
website: https://www.coininn.com
repository: 
issue: 
icon: com.coininn.yykik.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-09-28
signer: 
reviewArchive: 
twitter: coin_inn
social:
- https://www.linkedin.com/company/coininnwealthuab
- https://www.facebook.com/groups/140841034691073
- https://discord.com/invite/kk9yYxUB
features: 
developerName: CoinInn Wealth UAB

---

{% include copyFromAndroid.html %}
