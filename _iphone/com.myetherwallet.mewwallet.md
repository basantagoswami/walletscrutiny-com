---
wsId: mewEthereum
title: 'MEW crypto wallet: DeFi Web3'
altTitle: 
authors:
- danny
appId: com.myetherwallet.mewwallet
appCountry: us
idd: 1464614025
released: 2020-03-12
updated: 2024-03-01
version: 2.4.12
stars: 4.6
reviews: 4988
size: '188616704'
website: http://mewwallet.com
repository: 
issue: 
icon: com.myetherwallet.mewwallet.jpg
bugbounty: 
meta: removed
verdict: nobtc
date: 2024-05-07
signer: 
reviewArchive: 
twitter: myetherwallet
social:
- https://www.linkedin.com/company/myetherwallet
- https://www.facebook.com/MyEtherWallet
- https://www.reddit.com/r/MyEtherWallet
features: 
developerName: MyEtherWallet, Inc.

---

{% include copyFromAndroid.html %}