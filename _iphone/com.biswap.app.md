---
wsId: biswapCryptoApp
title: Biswap
altTitle: 
authors:
- danny
appId: com.biswap.app
appCountry: ca
idd: '1634279692'
released: 2022-11-29
updated: 2023-07-11
version: 1.0.12
stars: 5
reviews: 5
size: '39329792'
website: https://biswap.com
repository: 
issue: 
icon: com.biswap.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-09
signer: 
reviewArchive: 
twitter: exchangilydex
social:
- https://www.facebook.com/BestDEX
- https://t.me/exchangily_chat
- https://www.tiktok.com/@exchangilydex
features: 
developerName: Exchangily LLC

---

{% include copyFromAndroid.html %}