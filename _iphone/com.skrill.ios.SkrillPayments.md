---
wsId: skrill
title: Skrill - Wallet App
altTitle: 
authors:
- danny
appId: com.skrill.ios.SkrillPayments
appCountry: gb
idd: '718248239'
released: 2013-10-29
updated: 2024-05-20
version: 3.135.0
stars: 4.5
reviews: 6356
size: '161531904'
website: https://www.skrill.com/
repository: 
issue: 
icon: com.skrill.ios.SkrillPayments.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-11-17
signer: 
reviewArchive: 
twitter: skrill
social:
- https://www.facebook.com/Skrillpayments
- https://www.instagram.com/skrill
features: 
developerName: Skrill Ltd.

---

{% include copyFromAndroid.html %}
