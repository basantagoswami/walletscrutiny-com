---
wsId: ventAfrica
title: Vent Africa - Crypto to Cash
altTitle: 
authors:
- danny
appId: com.ventafrica.app
appCountry: us
idd: '1607187419'
released: 2022-02-01
updated: 2024-02-21
version: 1.8.4
stars: 3.4
reviews: 55
size: '81026048'
website: 
repository: 
issue: 
icon: com.ventafrica.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-10
signer: 
reviewArchive: 
twitter: ventafricaHQ
social:
- https://vent.africa
- https://t.me/ventafrica
features: 
developerName: Vent Africa

---

{% include copyFromAndroid.html %}
