---
wsId: tokero
title: TOKERO
altTitle: 
authors:
- danny
appId: com.ldvtech.tokero
appCountry: ro
idd: '1569586581'
released: 2021-06-04
updated: 2023-11-29
version: 4.0.3
stars: 4.7
reviews: 18
size: '22760448'
website: https://tokero.com
repository: 
issue: 
icon: com.ldvtech.tokero.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-28
signer: 
reviewArchive: 
twitter: tokeroexchange
social:
- https://www.facebook.com/TokeroExchange/
- https://www.linkedin.com/company/tokero-crypto-exchange/
- https://www.instagram.com/tokerocryptoexchange/
- https://www.youtube.com/channel/UC7ayrHLw7VqgsQUGP9ZKCIw
features: 
developerName: Globe Monnaie SRL

---

{% include copyFromAndroid.html %}
