---
wsId: DiviWallet
title: 'Divi Wallet: Crypto & Staking'
altTitle: 
authors:
- danny
appId: io.divipay.divi
appCountry: gb
idd: 1516551223
released: 2021-10-08
updated: 2023-09-03
version: 2.2.01215
stars: 4.6
reviews: 27
size: '61079552'
website: http://wallet.diviproject.org
repository: 
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/366
icon: io.divipay.divi.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-04
signer: 
reviewArchive: 
twitter: diviproject
social:
- https://www.facebook.com/diviproject
- https://www.reddit.com/r/DiviProject
- https://github.com/DiviProject
features: 
developerName: Qbito Technologies S.A.

---

{% include copyFromAndroid.html %}
