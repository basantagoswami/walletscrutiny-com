---
wsId: bitazza
title: 'Bitazza TH: Crypto Exchange'
altTitle: 
authors:
- danny
appId: com.bitazza.ios
appCountry: th
idd: 1476944844
released: 2020-05-25
updated: 2024-05-23
version: 3.7.4
stars: 4.1
reviews: 1033
size: '175310848'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: bitazzaofficial
social:
- https://www.linkedin.com/company/bitazza
- https://www.facebook.com/bitazza
features: 
developerName: Bitazza Company Limited

---

{% include copyFromAndroid.html %}
