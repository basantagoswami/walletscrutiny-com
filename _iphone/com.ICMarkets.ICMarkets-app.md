---
wsId: iCMarkets
title: IC Markets
altTitle: 
authors:
- danny
appId: com.ICMarkets.ICMarkets-app
appCountry: gb
idd: '1552875348'
released: 2021-02-24
updated: 2022-06-09
version: 1.1.2
stars: 3.2
reviews: 23
size: '25951232'
website: 
repository: 
issue: 
icon: com.ICMarkets.ICMarkets-app.jpg
bugbounty: 
meta: stale
verdict: nosendreceive
date: 2023-07-08
signer: 
reviewArchive: 
twitter: IC_Markets
social:
- https://icmarkets.com
- https://www.linkedin.com/company/icmarkets
features: 
developerName: International Capital Markets Pty Ltd

---

{% include copyFromAndroid.html %}
