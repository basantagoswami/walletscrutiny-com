---
wsId: bitazzaGL
title: Bitazza
altTitle: 
authors:
- danny
appId: com.bitazza.global.ios
appCountry: th
idd: '1612226119'
released: 2022-04-07
updated: 2024-05-13
version: 3.7.2
stars: 4.2
reviews: 63
size: '173999104'
website: https://www.bitazza.com
repository: 
issue: 
icon: com.bitazza.global.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-09
signer: 
reviewArchive: 
twitter: BitazzaGlobal
social:
- https://www.facebook.com/bitazzaglobal
- https://www.linkedin.com/company/bitazza/
- https://t.me/bitazzaglobal
features: 
developerName: Bitazza Company Limited

---

{% include copyFromAndroid.html %}
