---
wsId: 
title: 'Vivid: Mobile Banking'
altTitle: 
authors: 
appId: com.vivid.money
appCountry: me
idd: 1504417378
released: 2020-10-09
updated: 2023-02-22
version: 2.49.0
stars: 0
reviews: 0
size: '331750400'
website: https://vivid.money/
repository: 
issue: 
icon: com.vivid.money.jpg
bugbounty: 
meta: removed
verdict: wip
date: 2023-05-30
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Vivid Money GmbH

---

