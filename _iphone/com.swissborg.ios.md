---
wsId: swissborg
title: 'SwissBorg: Buy Bitcoin, crypto'
altTitle: 
authors:
- danny
appId: com.swissborg.ios
appCountry: gb
idd: 1442483481
released: 2020-03-31
updated: 2024-05-06
version: 1.84.0
stars: 4.4
reviews: 1958
size: '129400832'
website: https://swissborg.com
repository: 
issue: 
icon: com.swissborg.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-15
signer: 
reviewArchive: 
twitter: swissborg
social:
- https://www.linkedin.com/company/swissborg
- https://www.facebook.com/swissborg
features: 
developerName: SBorg SA

---

 {% include copyFromAndroid.html %}