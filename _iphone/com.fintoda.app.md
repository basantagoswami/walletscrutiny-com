---
wsId: fintodaWallet
title: Fintoda Wallet
altTitle: 
authors:
- danny
appId: com.fintoda.app
appCountry: us
idd: '1582196311'
released: 2021-09-02
updated: 2023-12-21
version: 1.5.5
stars: 0
reviews: 0
size: '31092736'
website: 
repository: 
issue: 
icon: com.fintoda.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-30
signer: 
reviewArchive: 
twitter: 
social:
- https://fintoda.com
features: 
developerName: FINTODA LLC

---

{% include copyFromAndroid.html %}