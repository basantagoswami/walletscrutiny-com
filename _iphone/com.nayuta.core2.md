---
wsId: nayutaCore2
title: Nayuta Wallet
altTitle: 
authors:
- danny
appId: com.nayuta.core2
appCountry: us
idd: '6449242331'
released: 2023-08-21
updated: 2023-11-14
version: 1.3.0
stars: 0
reviews: 0
size: '76743680'
website: https://nayuta.co/nayuta-wallet-2022
repository: 
issue: 
icon: com.nayuta.core2.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2024-01-18
signer: 
reviewArchive: 
twitter: 
social: 
features:
- ln
developerName: Nayuta

---

{% include copyFromAndroid.html %}