---
wsId: thorWalletDeFi
title: 'THORWallet: DeFi Crypto Wallet'
altTitle: 
authors:
- danny
appId: defisuisseag.thorwallet
appCountry: ch
idd: '1592064324'
released: 2021-11-28
updated: 2024-05-22
version: 2.1.15
stars: 4.8
reviews: 36
size: '64500736'
website: http://thorwallet.org
repository: 
issue: 
icon: defisuisseag.thorwallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-06-30
signer: 
reviewArchive: 
twitter: THORWalletDEX
social:
- https://t.me/THORWalletOfficial
- https://discord.com/invite/TArAZHDjCr
- https://thorwallet.medium.com
features: 
developerName: DeFi Suisse AG

---

{% include copyFromAndroid.html %}
