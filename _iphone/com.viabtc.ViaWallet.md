---
wsId: ViaWallet
title: CoinEx Wallet - Crypto & DeFi
altTitle: 
authors:
- leo
appId: com.viabtc.ViaWallet
appCountry: 
idd: 1462031389
released: 2019-05-21
updated: 2024-05-13
version: 4.3.0
stars: 4.1
reviews: 48
size: '91709440'
website: https://wallet.coinex.com/
repository: 
issue: 
icon: com.viabtc.ViaWallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-01
signer: 
reviewArchive: 
twitter: viawallet
social:
- https://www.facebook.com/ViaWallet
features: 
developerName: Coinex Global Limited

---

{% include copyFromAndroid.html %}
