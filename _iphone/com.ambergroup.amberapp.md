---
wsId: ambercrypto
title: 'WhaleFin: buy Crypto, BTC, ETH'
altTitle: 
authors:
- danny
appId: com.ambergroup.amberapp
appCountry: us
idd: 1515652068
released: 2020-09-21
updated: 2024-05-01
version: 2.18.3
stars: 4.5
reviews: 196
size: '240706560'
website: https://www.whalefin.com
repository: 
issue: 
icon: com.ambergroup.amberapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-09-11
signer: 
reviewArchive: 
twitter: ambergroup_io
social:
- https://www.linkedin.com/company/amberbtc
- https://www.facebook.com/ambergroup.io
features: 
developerName: AMBER AI LIMITED

---

{% include copyFromAndroid.html %}
