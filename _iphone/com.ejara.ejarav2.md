---
wsId: ejara
title: Ejara
altTitle: 
authors:
- danny
appId: com.ejara.ejarav2
appCountry: fr
idd: '1541127587'
released: 2021-03-22
updated: 2024-05-15
version: 3.6.4+304
stars: 3.9
reviews: 51
size: '201930752'
website: https://www.ejara.io
repository: 
issue: 
icon: com.ejara.ejarav2.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-02-21
signer: 
reviewArchive: 
twitter: EjaraApp
social:
- https://www.facebook.com/Ejaracapital
features: 
developerName: Ejara

---

{% include copyFromAndroid.html %}
