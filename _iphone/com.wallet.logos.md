---
wsId: LogosDVWallet
title: Dives Wallet
altTitle: 
authors:
- danny
appId: com.wallet.logos
appCountry: us
idd: '1537557477'
released: 2020-11-08
updated: 2023-07-05
version: v0.2.5
stars: 1
reviews: 1
size: '47070208'
website: https://logos-foundation.org/logos
repository: 
issue: 
icon: com.wallet.logos.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-07-06
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: LOGOS FOUNDATION PTE. LTD.

---

{% include copyFromAndroid.html %}