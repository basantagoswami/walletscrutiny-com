---
wsId: coinCatchExchange
title: CoinCatch- Buy Bitcoin &Crypto
altTitle: 
authors:
- danny
appId: com.coincatch.global
appCountry: us
idd: '6449619148'
released: 2023-07-05
updated: 2024-05-15
version: 1.11.0
stars: 4.5
reviews: 12
size: '122787840'
website: 
repository: 
issue: 
icon: com.coincatch.global.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-01
signer: 
reviewArchive: 
twitter: coincatchcom
social:
- https://www.coincatch.com
- https://t.me/coincatch_chat_en
- https://www.instagram.com/coincatch_official
features: 
developerName: Linkbase Technology Limited

---

{% include copyFromAndroid.html %}