---
wsId: edge
title: Edge - Crypto & Bitcoin Wallet
altTitle: 
authors:
- leo
- emanuel
appId: co.edgesecure.app
appCountry: 
idd: 1344400091
released: 2018-02-09
updated: 2024-05-14
version: 4.6.0
stars: 4.4
reviews: 1465
size: '96677888'
website: https://edge.app
repository: https://github.com/EdgeApp/edge-react-gui
issue: 
icon: co.edgesecure.app.jpg
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2022-03-13
signer: 
reviewArchive:
- date: 2019-11-10
  version: 1.10.1
  appHash: 
  gitRevision: 1707808e9efc2ab4ea3a03510ebd408811586d47
  verdict: ftbfs
twitter: edgewallet
social:
- https://www.linkedin.com/company/edgeapp
- https://www.reddit.com/r/EdgeWallet
features: 
developerName: Airbitz Inc

---

{% include copyFromAndroid.html %}
