---
wsId: foldapp
title: 'Fold: Earn and buy bitcoin'
altTitle: 
authors:
- danny
appId: com.foldapp
appCountry: us
idd: 1480424785
released: 2019-11-18
updated: 2024-05-21
version: 139.28.0
stars: 4.3
reviews: 1877
size: '111340544'
website: http://foldapp.com
repository: 
issue: 
icon: com.foldapp.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-09-15
signer: 
reviewArchive: 
twitter: fold_app
social: 
features: 
developerName: Fold

---

{% include copyFromAndroid.html %}
