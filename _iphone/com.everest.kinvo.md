---
wsId: everestKinvo
title: 'Kinvo: otimize investimentos'
altTitle: 
authors:
- danny
appId: com.everest.kinvo
appCountry: BR
idd: '1327335329'
released: 2018-01-03
updated: 2024-05-22
version: 3.40.0
stars: 4.6
reviews: 17247
size: '81027072'
website: http://www.kinvo.com.br
repository: 
issue: 
icon: com.everest.kinvo.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-23
signer: 
reviewArchive: 
twitter: kinvoapp
social:
- https://www.linkedin.com/company/kinvoapp
- https://www.facebook.com/appkinvo
- https://www.instagram.com/kinvoapp
- https://www.youtube.com/kinvoapp
features: 
developerName: EVEREST.TI

---

{% include copyFromAndroid.html %}
