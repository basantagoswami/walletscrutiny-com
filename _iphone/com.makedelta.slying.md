---
wsId: makeDelta
title: TraderCat - Alerts, Screener
altTitle: 
authors:
- danny
appId: com.makedelta.slying
appCountry: kr
idd: '1581110050'
released: 2021-08-15
updated: 2024-04-30
version: 8.0.1
stars: 4.7
reviews: 117
size: '57303040'
website: https://traderkat.io/
repository: 
issue: 
icon: com.makedelta.slying.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-14
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/profile.php?id=100083754173968
- https://www.youtube.com/channel/UCLC_CKhMggklpoHowc6TvNA
features: 
developerName: Make Delta Co., Ltd.

---

{% include copyFromAndroid.html %}
