---
wsId: BitcoinSuisse
title: Bitcoin Suisse
altTitle: 
authors:
- danny
appId: com.bitcoinsuisse.online
appCountry: pl
idd: 1555493299
released: 2021-09-21
updated: 2024-05-18
version: 2.12.1
stars: 0
reviews: 0
size: '21097472'
website: https://www.bitcoinsuisse.com/mobile-app
repository: 
issue: 
icon: com.bitcoinsuisse.online.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-11-22
signer: 
reviewArchive: 
twitter: BitcoinSuisseAG
social:
- https://www.linkedin.com/company/bitcoin-suisse-ag
- https://www.facebook.com/BitcoinSuisse
features: 
developerName: Bitcoin Suisse

---

{% include copyFromAndroid.html %}
