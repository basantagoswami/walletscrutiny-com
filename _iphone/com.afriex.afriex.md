---
wsId: Afriex
title: Afriex - Money transfer
altTitle: 
authors:
- danny
appId: com.afriex.afriex
appCountry: us
idd: 1492022568
released: 2020-03-06
updated: 2024-05-21
version: 11.104.1
stars: 4.8
reviews: 7813
size: '120563712'
website: https://afriexapp.com
repository: 
issue: 
icon: com.afriex.afriex.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-11-30
signer: 
reviewArchive: 
twitter: afriexapp
social:
- https://www.linkedin.com/company/afriex
- https://www.facebook.com/AfriexApp
features: 
developerName: Afriex Inc

---

{% include copyFromAndroid.html %}
