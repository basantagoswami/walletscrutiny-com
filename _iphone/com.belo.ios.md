---
wsId: beloCrypto
title: 'Belo: your financial passport'
altTitle: 
authors:
- danny
appId: com.belo.ios
appCountry: co
idd: '1575614708'
released: 2021-09-08
updated: 2024-05-19
version: 4.10.0
stars: 4.7
reviews: 15
size: '77087744'
website: https://belo.app
repository: 
issue: 
icon: com.belo.ios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-30
signer: 
reviewArchive: 
twitter: belo_app
social:
- https://www.linkedin.com/company/belo-app
- https://www.facebook.com/SomosBelo
- https://t.me/beloentelegram
- https://www.tiktok.com/@belo.app
features:
- ln
developerName: belo

---

{% include copyFromAndroid.html %}
