---
wsId: gopax
title: 고팍스 - GOPAX
altTitle: 
authors:
- danny
appId: kr.co.gopax
appCountry: kr
idd: 1369896843
released: 2018-06-21
updated: 2024-05-23
version: 2.6.1
stars: 2.8
reviews: 506
size: '152221696'
website: https://www.gopax.co.kr/notice
repository: 
issue: 
icon: kr.co.gopax.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-01
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Streami Inc.

---

 {% include copyFromAndroid.html %}
