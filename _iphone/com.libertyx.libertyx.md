---
wsId: libertyx
title: LibertyX - Buy Bitcoin
altTitle: 
authors:
- danny
appId: com.libertyx.libertyx
appCountry: us
idd: 966538981
released: 2015-02-20
updated: 2023-06-25
version: 4.1.3
stars: 3.7
reviews: 181
size: '14504960'
website: https://libertyx.com
repository: 
issue: 
icon: com.libertyx.libertyx.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-04
signer: 
reviewArchive: 
twitter: libertyx
social:
- https://www.linkedin.com/company/libertyx
- https://www.facebook.com/getlibertyx
features: 
developerName: Moon, Inc.

---

 {% include copyFromAndroid.html %}
