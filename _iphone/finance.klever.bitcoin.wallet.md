---
wsId: kleverK5FinanceCrypto
title: 'Klever Wallet: Bitcoin, Crypto'
altTitle: 
authors:
- danny
appId: finance.klever.bitcoin.wallet
appCountry: us
idd: '1615064243'
released: 2023-01-25
updated: 2024-05-21
version: 5.5.46
stars: 4.9
reviews: 736
size: '205353984'
website: https://klever.io
repository: 
issue: 
icon: finance.klever.bitcoin.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: klever_io
social:
- https://discord.gg/klever-io
- https://www.instagram.com/klever.io
- https://www.facebook.com/klever.io
features: 
developerName: Klever Exchange

---

{% include copyFromAndroid.html %}
