---
wsId: starwalletStarworks
title: StarWALLET 3.0
altTitle: 
authors:
- danny
appId: com.starworksglobal.starwallet2
appCountry: id
idd: '1601956892'
released: 2021-12-31
updated: 2024-04-09
version: 3.001.132
stars: 5
reviews: 11
size: '45601792'
website: https://www.starworksglobal.com/starwallet
repository: 
issue: 
icon: com.starworksglobal.starwallet2.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-08-14
signer: 
reviewArchive: 
twitter: starworksglobal
social:
- https://www.facebook.com/starworksglobal
- https://starworksglobal.medium.com
- https://www.linkedin.com/company/starworks-global-pte-ltd
- https://www.instagram.com/starworksglobal
- https://www.youtube.com/channel/UCty8czQ20jNHRB-cTeQrUDg
features: 
developerName: Starworks Global Pte.Ltd.

---

{% include copyFromAndroid.html %}