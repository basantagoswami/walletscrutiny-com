---
wsId: cashierestCrypto
title: Cashierest
altTitle: 
authors:
- danny
appId: kr.co.newlinkcorp.cashierest
appCountry: kr
idd: '1462245208'
released: 2019-09-29
updated: 2023-10-18
version: 4.24.3
stars: 4.3
reviews: 51
size: '59111424'
website: 
repository: 
issue: 
icon: kr.co.newlinkcorp.cashierest.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-21
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Newlink

---

{% include copyFromAndroid.html %}