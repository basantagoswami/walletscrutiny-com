---
wsId: igTradingPlatform
title: IG Trading Platform
altTitle: 
authors:
- danny
appId: com.igmarkets.ideal
appCountry: sg
idd: '406492428'
released: 2011-01-24
updated: 2024-05-23
version: 10.1814.0
stars: 4.5
reviews: 2629
size: '236230656'
website: http://www.ig.com
repository: 
issue: 
icon: com.igmarkets.ideal.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2022-06-23
signer: 
reviewArchive: 
twitter: IGInternationa1
social:
- https://www.linkedin.com/company/ig-international-
- https://www.facebook.com/IGInternational2
- https://www.youtube.com/channel/UCZj-ae-S_X-mocAH3xQnpUw
features: 
developerName: IG Group

---

{% include copyFromAndroid.html %}
