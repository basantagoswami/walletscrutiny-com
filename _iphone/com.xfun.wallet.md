---
wsId: xFunWallet
title: XFUN Wallet
altTitle: 
authors:
- danny
appId: com.xfun.wallet
appCountry: us
idd: '1612225910'
released: 2022-03-15
updated: 2023-07-20
version: 2.0.1
stars: 5
reviews: 2
size: '35380224'
website: 
repository: 
issue: 
icon: com.xfun.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-24
signer: 
reviewArchive: 
twitter: FUNtoken_io
social:
- https://xfun.io
- https://t.me/officialFUNToken
- https://discord.com/invite/e7vfgKbEKU
features: 
developerName: XFUN

---

{% include copyFromAndroid.html %}
