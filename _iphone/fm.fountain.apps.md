---
wsId: fountainPodcasts
title: 'Fountain: Podcast Player'
altTitle: 
authors:
- danny
appId: fm.fountain.apps
appCountry: ph
idd: '1576394424'
released: 2021-07-28
updated: 2024-05-06
version: 1.0.16
stars: 1
reviews: 1
size: '101422080'
website: https://www.fountain.fm
repository: 
issue: 
icon: fm.fountain.apps.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-24
signer: 
reviewArchive: 
twitter: fountain_app
social: 
features: 
developerName: Fountain Labs Ltd.

---

{% include copyFromAndroid.html %}