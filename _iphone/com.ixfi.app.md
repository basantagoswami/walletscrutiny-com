---
wsId: ixfiExchange
title: IXFI
altTitle: 
authors:
- danny
appId: com.ixfi.app
appCountry: ro
idd: '1614823165'
released: 2022-03-22
updated: 2024-05-24
version: 1.7.7
stars: 4.9
reviews: 97
size: '188305408'
website: https://www.ixfi.com/landing
repository: 
issue: 
icon: com.ixfi.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-08
signer: 
reviewArchive: 
twitter: ixfiexchange
social:
- https://www.linkedin.com/company/ixfiexchange
- https://www.facebook.com/ixfiexchange
- https://www.youtube.com/@ixfiexchange
- https://www.reddit.com/r/IXFIofficial
- https://ixfiexchange.medium.com
- https://t.me/ixfiexchange
- https://discord.com/invite/ixfiexchange
features: 
developerName: IXFI Crypto World UAB

---

{% include copyFromAndroid.html %}
