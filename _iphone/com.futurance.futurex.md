---
wsId: fexobitCrypto
title: Fexobit:Kripto Para |BTC|ETH
altTitle: 
authors:
- danny
appId: com.futurance.futurex
appCountry: tr
idd: '1583183728'
released: 2021-10-27
updated: 2024-05-21
version: 4.3.9
stars: 4.4
reviews: 110
size: '193890304'
website: 
repository: 
issue: 
icon: com.futurance.futurex.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: Fexobit
social:
- https://www.linkedin.com/company/fexobit
- https://www.facebook.com/Fexobit
- https://www.instagram.com/fexobit
- https://t.me/fexobit
- https://www.youtube.com/channel/UCwh5OepkJnVX1flXoyyPn2Q
features: 
developerName: Futurance Finans Teknolojileri A.S

---

{% include copyFromAndroid.html %}
