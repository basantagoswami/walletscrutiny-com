---
wsId: coinOneKr
title: 코인원
altTitle: 
authors:
- danny
appId: kr.co.coinone.officialapp
appCountry: kr
idd: 1326526995
released: 2018-03-27
updated: 2024-05-02
version: 4.12.0
stars: 2.7
reviews: 875
size: '128790528'
website: 
repository: 
issue: 
icon: kr.co.coinone.officialapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-17
signer: 
reviewArchive: 
twitter: CoinoneOfficial
social:
- https://coinone.co.kr
- https://www.facebook.com/coinone
features: 
developerName: Coinone

---

{% include copyFromAndroid.html %}