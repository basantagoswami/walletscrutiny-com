---
wsId: slighshotDeFi
title: Slingshot – DeFi Wallet
altTitle: 
authors:
- danny
appId: com.slingshot.finance
appCountry: us
idd: '1633406472'
released: 2023-02-28
updated: 2024-04-27
version: 1.19.7
stars: 4.8
reviews: 60
size: '61893632'
website: https://slingshot.finance/
repository: 
issue: 
icon: com.slingshot.finance.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-07-24
signer: 
reviewArchive: 
twitter: SlingshotCrypto
social:
- https://discord.com/invite/H3UcvgtASR
features: 
developerName: Slingshot Finance, Inc.

---

{% include copyFromAndroid.html %}
