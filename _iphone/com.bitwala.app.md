---
wsId: Nuri
title: Bitwala Bitcoin & Ether Wallet
altTitle: 
authors:
- danny
appId: com.bitwala.app
appCountry: gd
idd: 1454003161
released: 2019-05-11
updated: 2024-02-01
version: 3.5.1
stars: 0
reviews: 0
size: '62648320'
website: https://www.bitwala.com
repository: 
issue: 
icon: com.bitwala.app.jpg
bugbounty: 
meta: removed
verdict: nosource
date: 2024-03-02
signer: 
reviewArchive: 
twitter: nuribanking
social: 
features: 
developerName: Bitwala

---

{% include copyFromAndroid.html %}