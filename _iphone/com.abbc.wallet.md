---
wsId: AladdinWallet
title: Aladdin Wallet
altTitle: 
authors:
- danny
appId: com.abbc.wallet
appCountry: us
idd: 1475883958
released: 2019-08-16
updated: 2020-02-12
version: 1.3.3
stars: 5
reviews: 1
size: '52381696'
website: https://abbccoin.com/
repository: 
issue: 
icon: com.abbc.wallet.jpg
bugbounty: 
meta: obsolete
verdict: nosource
date: 2023-12-01
signer: 
reviewArchive: 
twitter: abbcfoundation
social:
- https://www.facebook.com/abbcfoundation/
- https://t.me/abbcfoundationofficial/
- https://t.me/abbcfoundation/
- https://www.instagram.com/abbccoin/
- https://www.linkedin.com/company/abbcfoundation/
- https://www.reddit.com/user/abbc_foundation/
features: 
developerName: MC ABBC IT SOLUTION

---

{% include copyFromAndroid.html %}
