---
wsId: COINiDwallet
title: Bitcoin Wallet for COINiD
altTitle: 
authors:
- leo
- danny
appId: org.coinid.wallet.btc
appCountry: 
idd: 1370200585
released: 2018-10-10
updated: 2022-11-19
version: 1.8.2
stars: 3.5
reviews: 23
size: '15645696'
website: https://coinid.org
repository: https://github.com/COINiD/COINiDWallet
issue: https://github.com/COINiD/COINiDWallet/issues/24
icon: org.coinid.wallet.btc.jpg
bugbounty: 
meta: stale
verdict: ftbfs
date: 2023-11-25
signer: 
reviewArchive: 
twitter: COINiDGroup
social: 
features: 
developerName: COINiD Group

---

{% include copyFromAndroid.html %}
