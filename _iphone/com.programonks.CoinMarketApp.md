---
wsId: DopamineBitcoin
title: Dopamine - Bitcoin & Crypto
altTitle: 
authors:
- danny
appId: com.programonks.CoinMarketApp
appCountry: us
idd: 1350234503
released: 2018-03-02
updated: 2024-05-21
version: 15.11.1
stars: 4.6
reviews: 620
size: '156735488'
website: https://www.dopamineapp.com/
repository: 
issue: 
icon: com.programonks.CoinMarketApp.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-10
signer: 
reviewArchive: 
twitter: mydopamineapp
social:
- https://www.facebook.com/myDopamineApp
features: 
developerName: CORTEX AG

---

{% include copyFromAndroid.html %}
