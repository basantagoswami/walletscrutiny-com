---
wsId: arcticCryptoWallet
title: Arctic Wallet Mobile
altTitle: 
authors:
- danny
appId: com.arcticmobile.app
appCountry: us
idd: '1658699963'
released: 2023-02-08
updated: 2024-05-12
version: '1.33'
stars: 4
reviews: 5
size: '58473472'
website: https://arcticwallet.io/
repository: 
issue: 
icon: com.arcticmobile.app.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-03
signer: 
reviewArchive: 
twitter: 
social:
- https://arcticwallet.io
- https://t.me/arctic_official_chat
- https://www.linkedin.com/company/arctic-wallet
- https://www.facebook.com/arcticwallet
- https://medium.com/@marketing_43986
features: 
developerName: Arctic Software OU

---

{% include copyFromAndroid.html %}
