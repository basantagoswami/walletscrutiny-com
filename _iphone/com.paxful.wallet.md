---
wsId: Paxful
title: 'Paxful: Buy Bitcoin & Ethereum'
altTitle: 
authors:
- leo
appId: com.paxful.wallet
appCountry: 
idd: 1443813253
released: 2019-05-09
updated: 2024-05-15
version: 2.9.3
stars: 3.4
reviews: 2976
size: '53137408'
website: https://paxful.com
repository: 
issue: 
icon: com.paxful.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: paxful
social:
- https://www.facebook.com/paxful
- https://www.reddit.com/r/paxful
features: 
developerName: Paxful Inc

---

{% include copyFromAndroid.html %}
