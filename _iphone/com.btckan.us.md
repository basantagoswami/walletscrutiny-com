---
wsId: BitKan
title: 'BitKan: Trade Bitcoin & Crypto'
altTitle: 
authors:
- danny
appId: com.btckan.us
appCountry: us
idd: 1004852205
released: 2015-06-24
updated: 2024-04-06
version: 8.20.3
stars: 3.5
reviews: 36
size: '148944896'
website: https://bitkan.com/
repository: 
issue: 
icon: com.btckan.us.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-21
signer: 
reviewArchive: 
twitter: bitkanofficial
social: 
features: 
developerName: BitKan Limited

---

{% include copyFromAndroid.html %}
