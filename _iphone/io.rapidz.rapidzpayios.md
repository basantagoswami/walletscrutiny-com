---
wsId: rapidzPay
title: Rapidz Pay
altTitle: 
authors:
- danny
appId: io.rapidz.rapidzpayios
appCountry: us
idd: '1558420115'
released: 2021-06-29
updated: 2024-05-21
version: 2.11.0
stars: 1
reviews: 1
size: '39825408'
website: https://www.rapidz.io/
repository: 
issue: 
icon: io.rapidz.rapidzpayios.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-19
signer: 
reviewArchive: 
twitter: Rapidz_io
social:
- https://t.me/RapidzCommunity
- https://www.instagram.com/rapidz_io
- https://www.facebook.com/Rapidz.io
features: 
developerName: UAB Rapidz Pay

---

{% include copyFromAndroid.html %}