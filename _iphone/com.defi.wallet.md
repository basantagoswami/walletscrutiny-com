---
wsId: cryptoComDefi
title: Crypto.com l DeFi Wallet
altTitle: 
authors:
- leo
appId: com.defi.wallet
appCountry: 
idd: 1512048310
released: 2020-05-20
updated: 2024-05-07
version: 1.82.0
stars: 4.7
reviews: 9397
size: '201831424'
website: https://crypto.com/defi-wallet
repository: 
issue: 
icon: com.defi.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: DeFi Labs

---

{% include copyFromAndroid.html %}
