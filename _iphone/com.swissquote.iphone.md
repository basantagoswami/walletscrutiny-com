---
wsId: swissQuoteTrading
title: Swissquote
altTitle: 
authors:
- danny
appId: com.swissquote.iphone
appCountry: ph
idd: '285691076'
released: 2019-02-12
updated: 2024-05-13
version: 10.9.1
stars: 4.2
reviews: 12
size: '95172608'
website: https://www.swissquote.com
repository: 
issue: 
icon: com.swissquote.iphone.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-20
signer: 
reviewArchive: 
twitter: Swissquote
social:
- https://www.linkedin.com/company/swissquote
- https://www.facebook.com/Swissquote.en
- https://www.youtube.com/user/SwissquoteBank
- https://www.tiktok.com/@swissquote_official
features: 
developerName: Swissquote

---

{% include copyFromAndroid.html %}