---
wsId: accrue
title: 'Accrue: Send. Spend. Sell.'
altTitle: 
authors:
- danny
appId: com.rocketsfintech.accrue-dca
appCountry: gh
idd: '1604973055'
released: 2022-01-18
updated: 2024-03-29
version: 3.3.8
stars: 4.4
reviews: 1511
size: '64231424'
website: https://useaccrue.com
repository: 
issue: 
icon: com.rocketsfintech.accrue-dca.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-29
signer: 
reviewArchive: 
twitter: useaccrue
social:
- https://www.instagram.com/useaccrue/
features: 
developerName: Accrue DCA Limited

---

{% include copyFromAndroid.html %}