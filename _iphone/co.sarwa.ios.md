---
wsId: sarwaApp
title: 'Sarwa: Invest, Trade & Save'
altTitle: 
authors:
- danny
appId: co.sarwa.ios
appCountry: us
idd: '1554353496'
released: 2021-04-12
updated: 2024-03-27
version: 5.3.1
stars: 4.2
reviews: 45
size: '79252480'
website: https://www.sarwa.co/blog
repository: 
issue: 
icon: co.sarwa.ios.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-28
signer: 
reviewArchive: 
twitter: SarwaCo
social:
- https://www.facebook.com/SarwaSocial
- https://www.linkedin.com/company/sarwa
- https://www.instagram.com/sarwa.co
features: 
developerName: Sarwa Digital Wealth Limited

---

{% include copyFromAndroid.html %}