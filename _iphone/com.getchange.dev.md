---
wsId: changeinvest
title: 'Change: Buy Bitcoin & crypto'
altTitle: 
authors:
- danny
appId: com.getchange.dev
appCountry: nl
idd: 1442085358
released: 2018-11-15
updated: 2024-05-16
version: 30.53.1
stars: 4.1
reviews: 37
size: '109900800'
website: https://www.changeinvest.com/
repository: 
issue: 
icon: com.getchange.dev.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2022-01-04
signer: 
reviewArchive: 
twitter: changefinance
social:
- https://www.linkedin.com/company/changeinvest
- https://www.facebook.com/changeinvest
features: 
developerName: xChange AS

---

{% include copyFromAndroid.html %}
