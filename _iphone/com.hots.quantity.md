---
wsId: hotscoin
title: HotsCoin — btc&eth量化策略自動交易數據平台
altTitle: 
authors:
- danny
appId: com.hots.quantity
appCountry: us
idd: '1628039456'
released: 2022-07-01
updated: 2022-07-29
version: 1.2.0
stars: 4.5
reviews: 10
size: '59738112'
website: 
repository: 
issue: 
icon: com.hots.quantity.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-02
signer: 
reviewArchive: 
twitter: hotscoin
social:
- https://hotscoin.com
features: 
developerName: 台湾凯旋网络科技

---

{% include copyFromAndroid.html %}