---
wsId: publicStocksCrypto
title: 'Public: Stocks, Options, Bonds'
altTitle: 
authors:
- danny
appId: com.t3securities.matador
appCountry: us
idd: '1204112719'
released: 2017-03-13
updated: 2024-05-21
version: 4.9.20
stars: 4.7
reviews: 69640
size: '247084032'
website: https://public.com
repository: 
issue: 
icon: com.t3securities.matador.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-21
signer: 
reviewArchive: 
twitter: public
social:
- https://www.instagram.com/publicapp
- https://www.facebook.com/PublicHello
- https://www.linkedin.com/company/publichello
- https://medium.com/the-public-blog
- https://www.tiktok.com/@public
features: 
developerName: Open to the Public Investing, Inc.

---

{% include copyFromAndroid.html %}