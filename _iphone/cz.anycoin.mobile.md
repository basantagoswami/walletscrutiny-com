---
wsId: anycoinCZ
title: 'Anycoin.cz: Crypto exchange'
altTitle: 
authors:
- danny
appId: cz.anycoin.mobile
appCountry: cz
idd: '1616670336'
released: 2022-04-25
updated: 2023-10-05
version: 1.24.0
stars: 4.1
reviews: 50
size: '32739328'
website: https://www.anycoin.cz
repository: 
issue: 
icon: cz.anycoin.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-05-03
signer: 
reviewArchive: 
twitter: anycoin_cz
social:
- https://www.facebook.com/anycoinCZ
features: 
developerName: MP Developers s.r.o.

---

{% include copyFromAndroid.html %}