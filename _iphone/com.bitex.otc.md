---
wsId: bitdataExchange
title: BitDATA Exchange (BitEx)
altTitle: 
authors:
- danny
appId: com.bitex.otc
appCountry: sg
idd: '1574033398'
released: 2021-09-02
updated: 2024-04-23
version: 1.8.3
stars: 5
reviews: 7
size: '56198144'
website: https://www.bitex.sg/
repository: 
issue: 
icon: com.bitex.otc.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-09-08
signer: 
reviewArchive: 
twitter: BitDATAExchange
social:
- https://www.linkedin.com/company/bitdataexchange
- https://www.facebook.com/BitDATAExchange/
features: 
developerName: BITDATA DIGITAL TECH PTE. LTD.

---

{% include copyFromAndroid.html %}