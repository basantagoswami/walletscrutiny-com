---
wsId: ecoTrader
title: Eco Trader
altTitle: 
authors:
- danny
appId: com.ecoonlineng.ecotrader
appCountry: us
idd: '1535823080'
released: 2020-11-05
updated: 2024-05-14
version: 4.9.1
stars: 3.6
reviews: 37
size: '21934080'
website: 
repository: 
issue: 
icon: com.ecoonlineng.ecotrader.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2024-03-30
signer: 
reviewArchive: 
twitter: 
social:
- https://ecoonlineng.com/
features: 
developerName: Uche Shadrach Onuigbo

---

{% include copyFromAndroid.html %}