---
wsId: cryptocomexchange
title: Crypto.com Exchange
altTitle: 
authors:
- leo
- danny
appId: crypto.com.exchange
appCountry: nz
idd: 1569309855
released: 2021-06-15
updated: 2024-05-21
version: 2.36.0
stars: 4.8
reviews: 82
size: '157872128'
website: https://crypto.com/exchange
repository: 
issue: 
icon: crypto.com.exchange.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-10-24
signer: 
reviewArchive: 
twitter: cryptocom
social:
- https://www.linkedin.com/company/cryptocom
- https://www.facebook.com/CryptoComOfficial
- https://www.reddit.com/r/Crypto_com
features: 
developerName: Crypto.com Exchange

---

{% include copyFromAndroid.html %}