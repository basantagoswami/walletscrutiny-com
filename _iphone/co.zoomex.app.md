---
wsId: zoomexCrypto
title: ZOOMEX - Trade&Invest Bitcoin
altTitle: 
authors:
- danny
appId: co.zoomex.app
appCountry: us
idd: '1601766234'
released: 2022-03-07
updated: 2024-05-23
version: 3.7.6
stars: 3.8
reviews: 13
size: '91873280'
website: 
repository: 
issue: 
icon: co.zoomex.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: zoomexofficial
social:
- https://www.zoomex.com
- https://t.me/zoomex_com
features: 
developerName: Octochain Fintech Limited

---

{% include copyFromAndroid.html %}
