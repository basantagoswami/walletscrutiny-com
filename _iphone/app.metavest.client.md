---
wsId: metavestCryptoBank
title: 'Metavest: Crypto Bank & Wallet'
altTitle: 
authors:
- danny
appId: app.metavest.client
appCountry: ph
idd: '1622578169'
released: 2022-07-28
updated: 2023-11-25
version: 2.0.1
stars: 0
reviews: 0
size: '41607168'
website: https://metavest.app/affiliate
repository: 
issue: 
icon: app.metavest.client.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-08
signer: 
reviewArchive: 
twitter: MetavestApp
social:
- https://metavest.app
- https://www.facebook.com/Metavestapp
- https://www.linkedin.com/company/metavestapp
- https://discord.com/invite/8sex2Upn79
- https://t.me/metavestofficial
- https://www.instagram.com/metavestapp
features: 
developerName: Metavest Limited

---

{% include copyFromAndroid.html %}