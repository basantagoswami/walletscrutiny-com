---
wsId: zkeExchange
title: ZKE - Buy Crypto
altTitle: 
authors:
- danny
appId: com.fmine.zkeApp
appCountry: us
idd: '6443931838'
released: 2022-11-09
updated: 2023-07-22
version: 5.8.1
stars: 4.8
reviews: 282
size: '174337024'
website: https://www.zke.com/
repository: 
issue: 
icon: com.fmine.zkeApp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-06-13
signer: 
reviewArchive: 
twitter: ZKE_com
social:
- https://www.youtube.com/channel/UCGe7ywdJ-0nNad4nbyPHGgg
- https://www.tiktok.com/@zkecom
- https://t.me/ZKEGlobal
- https://www.facebook.com/zkecom
features: 
developerName: fmine.com

---

{% include copyFromAndroid.html %}