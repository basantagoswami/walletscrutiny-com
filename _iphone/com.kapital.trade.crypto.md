---
wsId: bitcointradingcapital
title: Bitcoin trading - Capital.com
altTitle: 
authors:
- danny
appId: com.kapital.trade.crypto
appCountry: cz
idd: 1487443266
released: 2019-11-26
updated: 2024-05-16
version: 1.80.5
stars: 4.7
reviews: 781
size: '97461248'
website: https://capital.com/
repository: 
issue: 
icon: com.kapital.trade.crypto.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2024-02-08
signer: 
reviewArchive: 
twitter: capitalcom
social: 
features: 
developerName: CAPITAL BULGARIA

---

{% include copyFromAndroid.html %}
