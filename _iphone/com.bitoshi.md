---
wsId: bitoshiAfrica
title: 'Bitoshi: Buy & Sell Crypto'
altTitle: 
authors:
- danny
appId: com.bitoshi
appCountry: us
idd: '1627285591'
released: 2022-10-17
updated: 2024-05-14
version: 1.3.5
stars: 4.7
reviews: 362
size: '59189248'
website: 
repository: 
issue: 
icon: com.bitoshi.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-11-01
signer: 
reviewArchive: 
twitter: Bitoshiafrica
social:
- https://bitoshi.africa
- https://www.linkedin.com/company/bitoshiafrica
- https://www.instagram.com/bitoshi.africa
- https://t.me/+c4ek89XILkc2OTk8
features: 
developerName: Bitoshi Digital Services Limited

---

{% include copyFromAndroid.html %}