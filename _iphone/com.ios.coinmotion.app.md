---
wsId: Coinmotion
title: 'Coinmotion: Crypto Investing'
altTitle: 
authors:
- danny
appId: com.ios.coinmotion.app
appCountry: in
idd: 1518765595
released: 2020-11-19
updated: 2023-10-23
version: 1.10.0
stars: 0
reviews: 0
size: '35251200'
website: https://coinmotion.com/
repository: 
issue: 
icon: com.ios.coinmotion.app.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: Coinmotion
social:
- https://www.linkedin.com/company/coinmotion
- https://www.facebook.com/coinmotion
features: 
developerName: Coinmotion Oy

---

{% include copyFromAndroid.html %}
