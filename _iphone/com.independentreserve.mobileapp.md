---
wsId: independentReserveExchange
title: IR – Bitcoin & Crypto Exchange
altTitle: 
authors:
- danny
appId: com.independentreserve.mobileapp
appCountry: au
idd: '1566499416'
released: 2021-10-18
updated: 2024-05-16
version: 6.0.4
stars: 4.3
reviews: 184
size: '66223104'
website: https://www.independentreserve.com/
repository: 
issue: 
icon: com.independentreserve.mobileapp.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-01
signer: 
reviewArchive: 
twitter: indepreserve
social:
- https://www.linkedin.com/company/independent-reserve
- https://www.facebook.com/independentreserve
- https://www.reddit.com/r/independentreserve
- https://www.youtube.com/independentreserve
features: 
developerName: Independent Reserve

---

{% include copyFromAndroid.html %}
