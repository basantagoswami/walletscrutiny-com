---
wsId: atomic
title: Atomic Wallet
altTitle: 
authors:
- leo
appId: atomicwallet
appCountry: 
idd: 1478257827
released: 2019-11-05
updated: 2024-04-25
version: 1.29.5
stars: 4.4
reviews: 16603
size: '139353088'
website: https://atomicwallet.io/
repository: 
issue: 
icon: atomicwallet.jpg
bugbounty: 
meta: ok
verdict: obfuscated
date: 2023-11-22
signer: 
reviewArchive: 
twitter: atomicwallet
social:
- https://www.facebook.com/atomicwallet
features: 
developerName: ATOMIC PROTOCOL SYSTEMS OÜ

---

**Update 2022-01-02**: This app is no more available.

{% include copyFromAndroid.html %}
