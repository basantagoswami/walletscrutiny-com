---
wsId: alxCryptoWallet
title: ALX Crypto Wallet
altTitle: 
authors:
- danny
appId: com.prodoge.prodoge
appCountry: gb
idd: '1479083282'
released: 2019-09-23
updated: 2023-03-13
version: 3.3.15
stars: 2.4
reviews: 8
size: '71558144'
website: 
repository: 
issue: 
icon: com.prodoge.prodoge.jpg
bugbounty: 
meta: removed
verdict: nosource
date: 2024-04-03
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: 1FX LLC

---

{% include copyFromAndroid.html %}