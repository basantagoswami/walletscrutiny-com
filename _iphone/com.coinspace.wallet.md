---
wsId: coinspace
title: Coin Wallet - Bitcoin & Crypto
altTitle: 
authors:
- leo
appId: com.coinspace.wallet
appCountry: 
idd: 980719434
released: 2015-12-14
updated: 2024-05-14
version: 6.4.0
stars: 4.5
reviews: 275
size: '26389504'
website: https://coin.space/
repository: https://github.com/CoinSpace/CoinSpace
issue: 
icon: com.coinspace.wallet.jpg
bugbounty: https://www.openbugbounty.org//bugbounty/CoinAppWallet/
meta: ok
verdict: nonverifiable
date: 2023-04-22
signer: 
reviewArchive:
- date: 2019-12-16
  version: v2.16.3
  appHash: 
  gitRevision: 05400fa6155c33892a2955e12311ede0d86da12a
  verdict: ftbfs
twitter: coinappwallet
social:
- https://www.linkedin.com/company/coin-space
- https://www.facebook.com/coinappwallet
features: 
developerName: CoinSpace

---

{% include copyFromAndroid.html %}
