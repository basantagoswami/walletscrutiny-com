---
wsId: huobi
title: HTX:Buy BTC, Crypto Exchange
altTitle: 
authors:
- leo
appId: com.huobi.appStoreHuobiSystem
appCountry: 
idd: 1023263342
released: 2015-08-19
updated: 2024-03-07
version: 10.20.0
stars: 3.9
reviews: 1274
size: '261630976'
website: https://www.htx.com
repository: 
issue: 
icon: com.huobi.appStoreHuobiSystem.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-04-03
signer: 
reviewArchive: 
twitter: HuobiGlobal
social:
- https://www.facebook.com/huobiglobalofficial
features: 
developerName: Huobi LTD

---

Neither on Google Play nor on their website can we find a claim of a
non-custodial part to this app. We assume it is a purely custodial interface to
the exchange of same name and therefore **not verifiable**.
