---
wsId: kointra
title: Kointra
altTitle: 
authors:
- danny
appId: com.kointra.Kointra.V1.0
appCountry: tr
idd: '1486286836'
released: 2019-11-15
updated: 2024-01-21
version: 3.2.0
stars: 4.7
reviews: 30
size: '22765568'
website: https://kointra.com
repository: 
issue: 
icon: com.kointra.Kointra.V1.0.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-01-22
signer: 
reviewArchive: 
twitter: KointraTR
social:
- https://www.instagram.com/kointracom/
features: 
developerName: Kointra A.S

---

{% include copyFromAndroid.html %}