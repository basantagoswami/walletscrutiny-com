---
wsId: trubit
title: 'TruBit: Crypto Wallet'
altTitle: 
authors:
- danny
appId: com.oakk.starocket-wallet
appCountry: us
idd: '1612013344'
released: 2022-06-02
updated: 2024-05-22
version: 2.2.2
stars: 5
reviews: 6
size: '93563904'
website: https://trubit.com/
repository: 
issue: 
icon: com.oakk.starocket-wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-29
signer: 
reviewArchive: 
twitter: TruBit_Latam
social: 
features: 
developerName: TruBit Ltd.

---

{% include copyFromAndroid.html %}