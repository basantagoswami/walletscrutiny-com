---
wsId: Keystone
title: Keystone Hardware Wallet
altTitle: 
authors:
- danny
appId: keystone.mobile
appCountry: 
idd: 1567857965
released: 2021-06-03
updated: 2023-10-10
version: 1.3.4
stars: 2.7
reviews: 32
size: '34931712'
website: https://keyst.one/
repository: 
issue: 
icon: keystone.mobile.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2021-11-17
signer: 
reviewArchive: 
twitter: KeystoneWallet
social:
- https://github.com/KeystoneHQ
features: 
developerName: YANSSIE HK LIMITED

---

{% include copyFromAndroid.html %}
