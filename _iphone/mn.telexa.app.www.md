---
wsId: mnTelexa
title: Telexa.mn - Хөрөнгө оруулалт
altTitle: 
authors:
- danny
appId: mn.telexa.app.www
appCountry: mn
idd: '1596968900'
released: 2021-12-02
updated: 2023-11-01
version: '7.6'
stars: 4.7
reviews: 3210
size: '56816640'
website: https://www.telexa.mn/
repository: 
issue: 
icon: mn.telexa.app.www.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-17
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/Telexa.mn/
features: 
developerName: Save Inc.

---

{% include copyFromAndroid.html %}
