---
wsId: unitedExchange
title: Avner - United Exchange
altTitle: 
authors:
- danny
appId: com.vsmart.UnitedExchange
appCountry: az
idd: '1578830443'
released: 2021-07-30
updated: 2023-12-26
version: '11.1'
stars: 0
reviews: 0
size: '23066624'
website: https://unitedexchange.io
repository: 
issue: 
icon: com.vsmart.UnitedExchange.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-02-05
signer: 
reviewArchive: 
twitter: exchange_united
social:
- https://www.facebook.com/UnitedExchange.io
features: 
developerName: Avner Brokers

---

{% include copyFromAndroid.html %}
