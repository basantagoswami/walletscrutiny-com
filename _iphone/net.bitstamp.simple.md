---
wsId: bitstampCrypto
title: 'Bitstamp: Buy Crypto Simply'
altTitle: 
authors:
- danny
appId: net.bitstamp.simple
appCountry: us
idd: '1494703801'
released: 2023-07-20
updated: 2024-03-04
version: 1.8.2
stars: 4.7
reviews: 195
size: '118325248'
website: https://www.bitstamp.net/
repository: 
issue: 
icon: net.bitstamp.simple.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-14
signer: 
reviewArchive: 
twitter: Bitstamp
social:
- https://www.linkedin.com/company/bitstamp
- https://www.facebook.com/Bitstamp
- https://www.instagram.com/bitstampexchange
features: 
developerName: Bitstamp Ltd.

---

{% include copyFromAndroid.html %}