---
wsId: platnovaApp
title: Platnova
altTitle: 
authors:
- danny
appId: com.platnova.appin
appCountry: us
idd: '1619003446'
released: 2022-04-26
updated: 2024-05-14
version: 0.9.2
stars: 3.3
reviews: 41
size: '156088320'
website: https://platnova.com
repository: 
issue: 
icon: com.platnova.appin.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-02
signer: 
reviewArchive: 
twitter: getplatnova
social:
- https://www.facebook.com/getplatnova
- https://www.instagram.com/getplatnova
- https://www.linkedin.com/company/platnova
features: 
developerName: Platnova

---

{% include copyFromAndroid.html %}