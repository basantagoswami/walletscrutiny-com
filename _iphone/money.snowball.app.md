---
wsId: snowballDeFi
title: Snowball Smart DeFi Wallet
altTitle: 
authors:
- danny
appId: money.snowball.app
appCountry: us
idd: '1449662311'
released: 2019-07-18
updated: 2024-05-16
version: 3.3.0
stars: 4.3
reviews: 136
size: '39406592'
website: https://www.snowball.money
repository: 
issue: 
icon: money.snowball.app.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2023-04-15
signer: 
reviewArchive: 
twitter: snowball_money
social: 
features: 
developerName: Snowball Finance

---

{% include copyFromAndroid.html %}