---
wsId: speedBitcoinWallet
title: Speed Bitcoin Wallet
altTitle: 
authors:
- danny
appId: com.app.speed1
appCountry: us
idd: '6462426281'
released: 2023-08-24
updated: 2024-05-23
version: 1.10.0
stars: 4.6
reviews: 77
size: '81077248'
website: https://www.speed.app/
repository: 
issue: 
icon: com.app.speed1.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-03-02
signer: 
reviewArchive: 
twitter: speedwallet
social:
- https://www.linkedin.com/showcase/speedwallet
- https://www.instagram.com/speedbitcoinwallet
features: 
developerName: Speed1 - FZCO

---

{% include copyFromAndroid.html %}