---
wsId: sincereSWallet
title: S-Wallet
altTitle: 
authors:
- danny
appId: com.sincere.swallet
appCountry: ua
idd: '1594397830'
released: 2022-04-06
updated: 2023-04-24
version: 2.6.0
stars: 4.7
reviews: 173
size: '49532928'
website: https://s-wallet.ai
repository: 
issue: 
icon: com.sincere.swallet.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2024-04-18
signer: 
reviewArchive: 
twitter: SWallet_ai
social:
- https://www.facebook.com/SWallet.en
- https://t.me/SWallet_ai
features: 
developerName: Swallet OU

---

{% include copyFromAndroid.html %}

