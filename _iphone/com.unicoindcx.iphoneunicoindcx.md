---
wsId: unicoinDCXTrader
title: UnicoinDCX
altTitle: 
authors:
- danny
appId: com.unicoindcx.iphoneunicoindcx
appCountry: us
idd: '1447599401'
released: 2018-12-29
updated: 2024-04-22
version: 3.2.0
stars: 5
reviews: 3
size: '22172672'
website: 
repository: 
issue: 
icon: com.unicoindcx.iphoneunicoindcx.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2024-03-23
signer: 
reviewArchive: 
twitter: Unicoin5
social:
- https://www.facebook.com/unicoindcx
- https://www.linkedin.com/company/unicoindcx
- https://www.instagram.com/unicoindcx
- https://www.youtube.com/channel/UCVa0AyvJni57jdQ9jlGSUOw
features: 
developerName: UNICOIN DCX SDN. BHD.

---

{% include copyFromAndroid.html %}