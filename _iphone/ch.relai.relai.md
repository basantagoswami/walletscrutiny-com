---
wsId: relaiBuyBitcoin
title: 'Relai: Buy Bitcoin Easily'
altTitle: 
authors:
- danny
appId: ch.relai.relai
appCountry: ch
idd: '1513185997'
released: 2020-07-01
updated: 2024-05-22
version: 2.8.7
stars: 4.6
reviews: 1493
size: '75268096'
website: https://relai.app/
repository: 
issue: 
icon: ch.relai.relai.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: Relai_app
social:
- https://www.linkedin.com/company/relai-app
- https://www.youtube.com/channel/UCBtN1U9Aa7KgQeS_gJicUBw
- https://t.me/relai_en
- https://www.instagram.com/relai.app
features: 
developerName: Relai App GmbH

---

{% include copyFromAndroid.html %}
