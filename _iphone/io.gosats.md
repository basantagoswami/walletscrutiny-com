---
wsId: gosats
title: 'GoSats: Gold and BTC Rewards'
altTitle: 
authors:
- danny
appId: io.gosats
appCountry: in
idd: '1536263998'
released: 2021-01-05
updated: 2024-05-17
version: 2.4.10
stars: 4
reviews: 262
size: '45369344'
website: https://gosats.io
repository: 
issue: 
icon: io.gosats.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-03-23
signer: 
reviewArchive: 
twitter: gosatsapp
social: 
features: 
developerName: Saffron Technologies Pte Ltd

---

{% include copyFromAndroid.html %}