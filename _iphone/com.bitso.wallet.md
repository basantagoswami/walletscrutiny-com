---
wsId: bitso
title: Bitso - Buy bitcoin easily
altTitle: 
authors:
- leo
appId: com.bitso.wallet
appCountry: 
idd: 1292836438
released: 2018-02-19
updated: 2024-05-23
version: 3.61.3
stars: 4.6
reviews: 728
size: '184983552'
website: https://bitso.com/app
repository: 
issue: 
icon: com.bitso.wallet.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-25
signer: 
reviewArchive: 
twitter: Bitso
social:
- https://www.facebook.com/bitsoex
features: 
developerName: Bitso SAPI de CV

---

Bitso appears to be an exchange and their statement on security on their website

> **Maximum security**<br>
  We work every day to keep your account protected. That's why more than 2
  million users trust us.

is saying "trust us". Their security is **not verifiable**.
