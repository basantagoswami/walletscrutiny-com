---
wsId: wooXTrading
title: 'WOO X: Buy Crypto & BTC'
altTitle: 
authors:
- danny
appId: network.woo.mobile
appCountry: ph
idd: '1576648404'
released: 2021-09-17
updated: 2024-05-20
version: 3.27.0
stars: 5
reviews: 3
size: '103328768'
website: https://woo.org/
repository: 
issue: 
icon: network.woo.mobile.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-15
signer: 
reviewArchive: 
twitter: 
social: 
features: 
developerName: Wootech Limited

---

{% include copyFromAndroid.html %}
