---
wsId: trusteePlus
title: Trustee Plus | crypto neobank
altTitle: 
authors:
- danny
appId: com.trusteeplus1
appCountry: ma
idd: '1634455978'
released: 2022-07-18
updated: 2023-06-19
version: 1.14.1
stars: 0
reviews: 0
size: '153038848'
website: 
repository: 
issue: 
icon: com.trusteeplus1.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-09-15
signer: 
reviewArchive: 
twitter: TrusteeGlobal
social:
- https://t.me/trustee_ru
- https://www.youtube.com/@TrusteeGlobal
- https://www.instagram.com/trustee.global
features: 
developerName: Trustee Global

---

{% include copyFromAndroid.html %}