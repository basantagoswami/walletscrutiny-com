---
wsId: cmorqFinance
title: OpenFi, Web3 banking for all.
altTitle: 
authors:
- danny
appId: com.cmorq.hoopoe
appCountry: us
idd: '1530022030'
released: 2020-12-13
updated: 2024-05-24
version: 5.9.3
stars: 4.5
reviews: 161
size: '146754560'
website: https://www.cmorq.com/
repository: 
issue: 
icon: com.cmorq.hoopoe.jpg
bugbounty: 
meta: ok
verdict: nobtc
date: 2022-06-24
signer: 
reviewArchive: 
twitter: cmorq_
social:
- https://www.instagram.com/cmorq_/
- https://www.youtube.com/channel/UCzgf-7dC4hNbIb0TGtOuX6g
- https://www.tiktok.com/@_cmorq?
- https://www.facebook.com/DeFiBanking/
features: 
developerName: cmorq inc.

---

{% include copyFromAndroid.html %}
