---
wsId: btccLite
title: 'BTCC Lite: Trade Crypto & BTC'
altTitle: 
authors:
- danny
appId: com.btcc.BTCCReactNative
appCountry: us
idd: '1195786666'
released: 2017-01-20
updated: 2024-05-09
version: 2.6.1
stars: 4.3
reviews: 97
size: '134999040'
website: https://h5.btpiccdn.com/en-US/article/Customer_Service2
repository: 
issue: 
icon: com.btcc.BTCCReactNative.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-23
signer: 
reviewArchive: 
twitter: YourBTCC
social:
- https://www.linkedin.com/company/yourbtcc
- https://www.facebook.com/yourbtcc
- https://www.instagram.com/btccbitcoin
- https://www.youtube.com/c/BTCCOfficial2011
features: 
developerName: BTCC Limited

---

{% include copyFromAndroid.html %}