---
wsId: bitPlusbyWBTCb
title: Bit.plus by wBTCb
altTitle: 
authors:
- danny
appId: com.wbtcb.bitstock
appCountry: us
idd: '1508577020'
released: 2020-06-22
updated: 2024-04-17
version: 2.6.4
stars: 3.7
reviews: 3
size: '62254080'
website: https://bit.plus
repository: 
issue: 
icon: com.wbtcb.bitstock.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-11-17
signer: 
reviewArchive: 
twitter: 
social:
- https://www.bit.plus/
features: 
developerName: IP wBTCB solutions, s.r.o.

---

{% include copyFromAndroid.html %}

