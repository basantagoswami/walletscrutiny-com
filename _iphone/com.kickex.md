---
wsId: kickexExchange
title: KICKEX SECURELY CRYPTOCURRENCY
altTitle: 
authors:
- danny
appId: com.kickex
appCountry: us
idd: '1532751538'
released: 2022-06-15
updated: 2022-12-05
version: 1.9.2
stars: 0
reviews: 0
size: '99078144'
website: 
repository: 
issue: 
icon: com.kickex.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2024-04-19
signer: 
reviewArchive: 
twitter: kickexcom
social:
- https://kickex.com
- https://www.facebook.com/kickex.official
- https://t.me/KickICO
- https://kickecosystem.medium.com
features: 
developerName: Kickex

---

{% include copyFromAndroid.html %}
