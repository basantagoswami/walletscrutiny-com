---
wsId: krakent
title: 'Kraken Pro: Crypto Trading'
altTitle: 
authors:
- leo
appId: com.kraken.trade.app
appCountry: 
idd: 1473024338
released: 2019-11-12
updated: 2024-05-21
version: 4.18.0
stars: 4.6
reviews: 14377
size: '110703616'
website: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.app.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-14
signer: 
reviewArchive: 
twitter: krakenfx
social:
- https://www.linkedin.com/company/krakenfx
- https://www.facebook.com/KrakenFX
features:
- ln
developerName: Kraken

---

On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.
