---
wsId: bitcoinbeach
title: Blink (Bitcoin Beach Wallet)
altTitle: 
authors:
- danny
appId: io.galoy.bitcoinbeach
appCountry: ng
idd: '1531383905'
released: 2020-11-11
updated: 2024-05-15
version: 2.2.273
stars: 4.5
reviews: 24
size: '60402688'
website: https://blink.sv
repository: https://github.com/GaloyMoney/galoy-mobile/
issue: 
icon: io.galoy.bitcoinbeach.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-10-20
signer: 
reviewArchive: 
twitter: blinkbtc
social:
- >-
  https://api.whatsapp.com/send/?phone=50369835117&text&type=phone_number&app_absent=0
- https://t.me/blinkbtc
- https://snort.social/p/community@blink.sv
features:
- ln
developerName: Galoy Inc

---

{% include copyFromAndroid.html %}