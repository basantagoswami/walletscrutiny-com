---
wsId: tuxWallet
title: TUX WALLET
altTitle: 
authors:
- danny
appId: net.tuxglobal.wallet
appCountry: kw
idd: '1495945761'
released: 2020-02-04
updated: 2024-05-20
version: 1.9.0
stars: 0
reviews: 0
size: '39250944'
website: https://tux-wallet.com/
repository: 
issue: 
icon: net.tuxglobal.wallet.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-08-18
signer: 
reviewArchive: 
twitter: Coinyexdotcom
social:
- https://t.me/coinyexchannel
features: 
developerName: Coinyex Co., Ltd.

---

{% include copyFromAndroid.html %}
