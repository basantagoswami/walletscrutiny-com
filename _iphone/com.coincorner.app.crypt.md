---
wsId: coincorner
title: CoinCorner - Bitcoin Wallet
altTitle: 
authors:
- kiwilamb
appId: com.coincorner.app.crypt
appCountry: 
idd: 917721788
released: 2014-09-22
updated: 2023-09-04
version: 4.3.5
stars: 4
reviews: 10
size: '42601472'
website: https://www.coincorner.com
repository: 
issue: 
icon: com.coincorner.app.crypt.jpg
bugbounty: 
meta: removed
verdict: custodial
date: 2023-12-19
signer: 
reviewArchive: 
twitter: CoinCorner
social:
- https://www.facebook.com/CoinCorner
features:
- ln
developerName: CoinCorner Ltd

---

A search of the app store and the providers website, reveals no statements about how private keys are managed.

This leads us to conclude the wallets funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

