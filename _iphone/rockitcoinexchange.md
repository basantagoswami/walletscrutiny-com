---
wsId: rockitCoin
title: RockItCoin - Buy Bitcoin Now
altTitle: 
authors:
- danny
appId: rockitcoinexchange
appCountry: us
idd: '1476730078'
released: 2019-09-18
updated: 2024-05-21
version: 3.4.6
stars: 3.2
reviews: 130
size: '72211456'
website: https://rockitcoin.com
repository: 
issue: 
icon: rockitcoinexchange.jpg
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-07
signer: 
reviewArchive: 
twitter: rockitcoin
social:
- https://www.facebook.com/RockItCoin
- https://www.instagram.com/rockitcoin
features: 
developerName: RockitCoin

---

{% include copyFromAndroid.html %}
