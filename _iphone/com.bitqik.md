---
wsId: bitqikExchange
title: bitqik
altTitle: 
authors:
- danny
appId: com.bitqik
appCountry: us
idd: '1645041960'
released: 2022-09-26
updated: 2023-07-05
version: 2.1.0
stars: 3.7
reviews: 3
size: '32717824'
website: 
repository: 
issue: 
icon: com.bitqik.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-08-07
signer: 
reviewArchive: 
twitter: bitqikofficial
social:
- https://www.facebook.com/bitqik
- https://www.instagram.com/bitqikofficial
- https://t.me/bitqik
- https://www.tiktok.com/@bitqikofficial
- https://www.youtube.com/channel/UC2wf6B1zII-2jPtVfwmKvoA
features: 
developerName: BITQIK SOLE CO.,LTD

---

{% include copyFromAndroid.html %}