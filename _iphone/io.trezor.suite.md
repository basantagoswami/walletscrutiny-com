---
wsId: trezorSuiteLite
title: Trezor Suite Lite
altTitle: 
authors:
- danny
appId: io.trezor.suite
appCountry: us
idd: '1631884497'
released: 2023-05-14
updated: 2024-04-29
version: 24.4.2
stars: 3.2
reviews: 58
size: '52787200'
website: https://trezor.io/
repository: 
issue: 
icon: io.trezor.suite.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-18
signer: 
reviewArchive: 
twitter: trezor
social:
- https://www.reddit.com/r/TREZOR
- https://www.instagram.com/trezor.io
- https://www.facebook.com/trezor.io
features: 
developerName: Trezor Company s.r.o.

---

{% include copyFromAndroid.html %}
