---
wsId: junoFinance
title: Juno - Buy Bitcoin & Litecoin
altTitle: 
authors:
- danny
appId: com.capitalJ.onJuno
appCountry: us
idd: '1525858971'
released: 2021-02-13
updated: 2024-05-17
version: 4.1.4
stars: 3.8
reviews: 930
size: '306108416'
website: https://juno.finance
repository: 
issue: 
icon: com.capitalJ.onJuno.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-07-19
signer: 
reviewArchive: 
twitter: JunoFinanceHQ
social:
- https://www.linkedin.com/company/junofinancehq
features: 
developerName: CapitalJ Inc

---

{% include copyFromAndroid.html %}
