---
wsId: spednByFlexa
title: SPEDN by Flexa
altTitle: 
authors:
- danny
appId: com.spedn.spedn
appCountry: us
idd: '1456135087'
released: 2019-05-13
updated: 2023-11-22
version: 23.11.1
stars: 4.6
reviews: 79
size: '27427840'
website: https://spedn.io
repository: 
issue: 
icon: com.spedn.spedn.jpg
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2023-07-28
signer: 
reviewArchive: 
twitter: FlexaHQ
social:
- https://www.facebook.com/flexa
features: 
developerName: Flexa Inc.

---

{% include copyFromAndroid.html %}
