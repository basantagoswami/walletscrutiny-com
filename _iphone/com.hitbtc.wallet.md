---
wsId: HitBTCWallet
title: Blockchain Crypto coin Wallet
altTitle: 
authors:
- danny
appId: com.hitbtc.wallet
appCountry: au
idd: 1580572986
released: 2021-09-01
updated: 2022-12-20
version: 1.3.8
stars: 5
reviews: 1
size: '103245824'
website: https://hitbtc.com/wallet
repository: 
issue: 
icon: com.hitbtc.wallet.jpg
bugbounty: 
meta: stale
verdict: custodial
date: 2023-12-16
signer: 
reviewArchive: 
twitter: hitbtc
social:
- https://www.facebook.com/hitbtc
- https://www.reddit.com/r/hitbtc
features: 
developerName: HitBTC

---

{% include copyFromAndroid.html %}
