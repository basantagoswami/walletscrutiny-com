---
wsId: simpleswap
title: SimpleSwap - Crypto Exchange
altTitle: 
authors:
- danny
appId: com.simpleswap.app
appCountry: gb
idd: 1506038278
released: 2020-05-15
updated: 2024-05-20
version: 4.2.0
stars: 4.7
reviews: 53
size: '62653440'
website: https://simpleswap.io/mobile-app
repository: 
issue: 
icon: com.simpleswap.app.jpg
bugbounty: 
meta: ok
verdict: nowallet
date: 2021-10-01
signer: 
reviewArchive: 
twitter: SimpleSwap_io
social:
- https://www.facebook.com/SimpleSwap.io
- https://www.reddit.com/r/simpleswapexchange
features: 
developerName: Simple Swap LTD

---

 {% include copyFromAndroid.html %}
