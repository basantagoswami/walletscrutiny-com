---
wsId: trendofx
title: 'ترندو: بروکر فارکس،تریدر فارسی'
altTitle: 
authors:
- danny
appId: ios.m3.Trendo
appCountry: in
idd: 1530580389
released: 2020-09-29
updated: 2024-05-07
version: 3.5.68
stars: 5
reviews: 32
size: '56569856'
website: https://fxtrendo.com/
repository: 
issue: 
icon: ios.m3.Trendo.jpg
bugbounty: 
meta: ok
verdict: custodial
date: 2023-04-04
signer: 
reviewArchive: 
twitter: 
social:
- https://www.instagram.com/fxtrendo/
features: 
developerName: Trendo LLC

---

{% include copyFromAndroid.html %}
