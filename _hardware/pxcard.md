---
title: p(x)Card
appId: pxcard
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: FunctionX
providerWebsite: https://www.functionx.io/home
website: https://pxcard.io/
shop: 
country: 
price: 39.99USD
repository: 
issue: 
icon: pxcard.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-03-10
signer: 
reviewArchive: 
twitter: pxcard_official
social: 
features: 

---

## Overview

{{ page.title }} remains unreleased as off this date, but it recently held a [Presale Whitelisting Campaign](https://gleam.io/NuMHV/pxcard-presale-whitelisting-campaign-30-off-with-pundixfx) from Feb. 28, 2023 to Mar 06, 2023.

As of this review, it does not have an official store page yet.

{{ page.title }} does not have a screen or buttons for confirming transactions and it appears to be able to connect via NFC. 

## Verdict 

This is not yet released.
