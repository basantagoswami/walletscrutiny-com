---
title: Conflux Network BSIM
appId: confluxnetwork.bsim
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions: 
weight: 
provider: Conflux Network / China Telecom
providerWebsite: 
website: https://www.confluxnetwork.org
shop: 
country: CN
price: 
repository: 
issue: 
icon: confluxnetwork.bsim.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-12-07
signer: 
reviewArchive: 
twitter: Conflux_Network
social:
- https://t.me/Conflux_English
- https://www.reddit.com/r/Conflux_Network
- https://www.youtube.com/@confluxnetwork
- https://discord.com/invite/conflux-network-707952293412339843
features: 

---

## Product Description

The BSIM has been mentioned in an [article](https://www.coindesk.com/markets/2023/03/23/first-mover-asia-blockchain-enabled-sim-card-for-crypto-investors-fuels-conflux-growth-bitcoin-holds-near-273k-after-fed-decision/) on CoinDesk: 

- It has the security of a hardware wallet.
- It can manage and store both public and private keys.
- Has 10x the storage capacity of a "traditional" sim.
- It has been described that the private key will not exit the card.
- It would allow access to Web3

## Analysis 

It is now December 2023, and apart from numerous press-releases back in May, we have not seen the device in action. 
The latest information about the {{ page.title }} is the development of open APIs for the BSIM wallet, and the introduction of BSIM account support.

We will mark this as **unreleased** until further notice. Moreover, there is a high likelihood that this is **not Bitcoin-specific**. 
