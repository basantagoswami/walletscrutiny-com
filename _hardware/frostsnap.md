---
title: Frost Snap
appId: frostsnap
authors:
- danny
released: 2022-09-16
discontinued: 
updated: 2023-12-07
version: 
binaries: 
dimensions: 
weight: 
provider: FrostSnap Tech
providerWebsite: 
website: https://frostsnap.com
shop: 
country: 
price: 
repository: https://github.com/frostsnap/frostsnap
issue: 
icon: frostsnap.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2024-01-05
signer: 
reviewArchive: 
twitter: FrostsnapTech
social: 
features: 

---

## Product Description

> Connect one or more Frostsnap devices to your phone and easily create a Bitcoin wallet in our app.
>
> If you create a `2-of-3`, any two devices are required to access your wallet.
>
> You can geographically separate your Frostsnap devices or share them amongst individuals you trust.
>
> With Frostsnap, the strong security you need is now accessible.

## Analysis 

The device is **not yet commercially available**.