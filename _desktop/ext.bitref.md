---
title: BitRef Firefox Extension
appId: ext.bitref
authors:
- danny
released: 2017-03-28
discontinued: 
updated: 2020-06-08
version: 3.1.3
provider: 
providerWebsite: https://bitref.com
website: https://addons.mozilla.org/en-US/firefox/addon/bitref/
repository: 
issue: 
icon: ext.bitref.png
bugbounty: 
meta: obsolete
verdict: wip
date: 2024-05-09
reviewArchive:
twitter: 
social:
features:
---
