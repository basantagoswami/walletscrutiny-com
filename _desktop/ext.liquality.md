---
title: Liquality Chrome Extension
appId: ext.liquality
authors:
- danny
released: 2020-04-09
discontinued: 
updated: 2023-06-01
version: 0.87.4
provider: Liquality
providerWebsite: https://liquality.io
website: https://chromewebstore.google.com/detail/kpfopkelmapcoipemfendmdcghnegimn
repository: https://github.com/liquality/wallet
issue: 
icon: ext.liquality.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-09
reviewArchive:
twitter: Liquality_io
social:
features:
---
