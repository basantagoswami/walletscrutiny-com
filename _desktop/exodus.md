---
title: Exodus
appId: exodus
authors:
- danny
released: 2015-12-09
discontinued: 2024-04-22
updated: 
version: 24.17.5
provider: Exodus Movement, Inc. 
providerWebsite: 
website: https://www.exodus.com/
repository: 
issue: 
icon: exodus.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-04-25
reviewArchive:
twitter: exodus_io
social:
 - https://www.facebook.com/exodus.io
features:
---
