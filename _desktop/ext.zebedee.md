---
title: Zebedee Extension
appId: ext.zebedee
authors:
- danny
released: 
discontinued: 
updated: 2024-04-13
version: 1.0.4
provider: Zebedee
providerWebsite: 
website: https://chromewebstore.google.com/detail/zbd-browser-extension/kpjdchaapjheajadlaakiiigcbhoppda
repository: 
issue: 
icon: ext.zebedee.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-09
reviewArchive:
twitter: zbdapp
social:
- https://www.instagram.com/zbdapp
- https://www.tiktok.com/@zbdapp
- https://discord.com/invite/zbdapp
- https://www.youtube.com/@zbdapp
features:
---
