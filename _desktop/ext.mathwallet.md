---
title: MathWallet Extension for Chrome
appId: ext.mathwallet
authors:
- danny
released: 2020-07-15
discontinued: 
updated: 2024-04-24
version: 3.0.9
provider: 
providerWebsite: https://mathwallet.org
website: https://chromewebstore.google.com/detail/math-wallet/afbcbjpbpfadlkmhmclhkeeodmamcflc
repository: https://github.com/mathwallet/math-kusamajs
issue: 
icon: ext.mathwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2024-05-09
reviewArchive:
twitter: MathWallet
social:
features:
---

Has recent updates but the GitHub repository hasn't kept up with them.