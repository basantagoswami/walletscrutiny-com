---
title: SwiftCash Web Wallet
appId: web.swiftcash
authors:
- danny
released: 2020-03-28
discontinued: 
updated: 2024-02-10
version: 
provider: 
providerWebsite: 
website: https://wallet.swiftcash.cc
repository: https://github.com/swiftcashproject/webwallet
issue: 
icon: web.swiftcash.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: swiftcashcc
social:
features:
---
