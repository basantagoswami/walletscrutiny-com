---
title: Blockchair Extension for Chrome
appId: ext.blockchair
authors:
- danny
released: 2020-12-17
discontinued: 
updated: 2023-06-08
version: 2.1.21
provider: Blockchair
providerWebsite: https://blockchair.com
website: https://chromewebstore.google.com/detail/fhhkkooikehnkaodebbfnkinedlllcfk
repository: 
issue: 
icon: ext.blockchair.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-09
reviewArchive:
twitter: blockchair
social:
features:
---
