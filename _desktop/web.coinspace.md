---
title: CoinSpace Web Wallet
appId: web.coinspace
authors:
- danny
released: 2020-05-16
discontinued: 
updated: 2024-04-18
version: 6.3.0
provider: CoinSpace LLC
providerWebsite: 
website: https://coin.space
repository: https://github.com/CoinSpace
issue: 
icon: web.coinspace.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: CoinAppWallet
social:
features:
---
