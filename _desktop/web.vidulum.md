---
title: Vidulum App Multi-Asset Crypto Storage
appId: web.vidulum
authors:
- danny
released: 2019-01-13
discontinued: 
updated: 2019-04-21
version: 10.8
provider: 
providerWebsite: 
website: https://wallet.vidulum.app
repository: https://github.com/vidulum/vidulum.app
issue: 
icon: web.vidulum.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: VidulumApp
social:
features:
---
