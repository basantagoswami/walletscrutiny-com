---
title: Freewallet Web
appId: web.freewallet
authors:
- danny
released: 
discontinued: 
updated: 
version: 
provider: Wallet Services Ltd. 
providerWebsite: 
website: https://app.freewallet.org
repository: 
issue: 
icon: web.freewallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: freewalletorg
social:
features:
---
