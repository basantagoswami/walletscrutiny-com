---
title: Coinb.in Web Wallet
appId: web.coinb.in
authors:
- danny
released: 2015-03-25
discontinued: 
updated: 2020-07-07
version: 1.7-beta
provider: OutCast3k
providerWebsite: 
website: https://coinb.in
repository: https://github.com/OutCast3k/coinbin
issue: 
icon: web.coinb.in.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: 
social:
features:
---
