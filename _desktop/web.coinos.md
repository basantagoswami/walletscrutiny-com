---
title: Coinos
appId: web.coinos
authors:
- danny
released: 2012-09-06
discontinued: 
updated: 2024-05-07
version: 
provider: 
providerWebsite: 
website: https://coinos.io
repository: https://github.com/coinos/coinos-ui
issue: 
icon: web.coinos.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-10
reviewArchive:
twitter: coinoswallet
social:
features:
---
