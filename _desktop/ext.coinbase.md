---
title: Coinbase Wallet Extension for Chrome
appId: ext.coinbase
authors:
- danny
released: 2021-05-18
discontinued: 
updated: 2024-05-09
version: 3.67.0
provider: Toshi Holdings Pte. Ltd
providerWebsite: https://coinbase.com
website: https://chromewebstore.google.com/detail/hnfanknocfeofbddgcijnmhnfnkdnaad
repository: 
issue: 
icon: ext.coinbase.png
bugbounty: 
meta: ok
verdict: wip
date: 2024-05-09
reviewArchive:
twitter: coinbase
social:
- https://www.linkedin.com/company/coinbase
- https://www.facebook.com/Coinbase
- https://medium.com/the-coinbase-blog/
features:
---
