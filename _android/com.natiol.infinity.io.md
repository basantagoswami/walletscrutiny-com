---
wsId: natiolInfinity
title: Natiol Infinity
altTitle: 
authors:
- danny
users: 10000
appId: com.natiol.infinity.io
appCountry: 
released: 2022-11-12
updated: 2024-04-06
version: 10.0.0
stars: 4.8
ratings: 
reviews: 4
size: 
website: https://natiol.io/
repository: 
issue: 
icon: com.natiol.infinity.io.png
bugbounty: 
meta: ok
verdict: nobtc
date: 2024-01-09
signer: 
reviewArchive: 
twitter: natiolinfinity
social:
- https://natiol.io
- https://www.facebook.com/natiol.io
- https://t.me/natiol_infinity
redirect_from: 
developerName: NATIOL INFINITY PTE LTD
features: 

---

This app replaces the {% include walletLink.html wallet='android/io.natiol' verdict='true' %} app.

## App Description

The app description describes the Natiol corporate entity. It is primarily related to DeFi and staking. 

## Analysis 

We installed the app and we signed up. Our login credentials are reflected on the web platform. 

We were not given a seed phrase and we could not find a BTC wallet. There is an option to "stake BTC" but we do not see an address. When we try to open the app settings, the app automatically closes. 

We do **not see a Bitcoin wallet** in this app.