---
wsId: TradeSanta
title: 'TradeSanta: Crypto Trading Bot'
altTitle: 
authors:
- danny
users: 50000
appId: com.tradesanta
appCountry: us
released: 2019-05-20
updated: 2022-12-05
version: 3.1.5
stars: 3.6
ratings: 707
reviews: 56
size: 
website: https://tradesanta.com
repository: 
issue: 
icon: com.tradesanta.png
bugbounty: 
meta: stale
verdict: nowallet
date: 2023-12-01
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: TradeSanta - Crypto Trading Bot
features: 

---

## App Description and Verdict
TradeSanta isn't a cryptocurrency exchange or wallet, but claims to provide trading bots that automate trading strategies on crypto exchanges, including:

> ➡️ Binance exchange, Binance US, Binance futures trading, HitBTC, Okex, Huobi and UpBit

It also has the following instructions for an easy setup:

> - **Connect to a supported exchange via API**
> - Choose a long/short template or create a trading bot from scratch
> - Select a pair
> - Fine-tune crypto bot’s parameters

TradeSanta clearly is **not a bitcoin wallet.**
