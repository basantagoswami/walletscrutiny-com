---
wsId: 
title: Bitcoin OX — Crypto Wallet
altTitle: 
authors:
- leo
users: 1000
appId: org.bitcoinox.bitcoinoxwallet
appCountry: 
released: 2019-02-04
updated: 2023-05-25
version: 2.8.7
stars: 3.9
ratings: 32
reviews: 4
size: 
website: https://bitcoinox.com
repository: 
issue: 
icon: org.bitcoinox.bitcoinoxwallet.png
bugbounty: 
meta: stale
verdict: nosource
date: 2024-05-24
signer: 
reviewArchive: 
twitter: bitcoin_ox
social:
- https://www.facebook.com/bitcoinoxwallet
redirect_from:
- /org.bitcoinox.bitcoinoxwallet/
- /posts/org.bitcoinox.bitcoinoxwallet/
developerName: Excdev
features: 

---

> Safety
> 
> • Due to Bitcoin OX, only you are in control of your Private Keys and manage
>   your Digital Assets
> 
> • No copies on our or public servers
> 
> • Compatible with BIP39 mnemonic code for generating deterministic keys

sounds like a non-custodial wallet but as there is no public source code linked
on their Google Play description or their website. This wallet is **not verifiable**.
