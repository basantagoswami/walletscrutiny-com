---
wsId: webull
title: 'Webull: Investing & Trading'
altTitle: 
authors:
- danny
- leo
users: 10000000
appId: org.dayup.stocks
appCountry: 
released: 
updated: 2024-05-21
version: 10.1.0.83
stars: 4.3
ratings: 165702
reviews: 39427
size: 
website: https://www.webull.com
repository: 
issue: 
icon: org.dayup.stocks.png
bugbounty: 
meta: ok
verdict: nosendreceive
date: 2021-08-10
signer: 
reviewArchive: 
twitter: WebullGlobal
social:
- https://www.linkedin.com/company/webullfinancialllc
redirect_from: 
developerName: Webull Technologies Pte. Ltd.
features: 

---

The app allows you to trade BTC but it is only an exchange and not a wallet.

From their website:
> We provide our customers with access to cryptocurrency trading through Apex Crypto. Apex Crypto is not a registered broker-dealer or FINRA member and your cryptocurrency holdings are not FDIC or SIPC insured.

> You can buy and sell cryptocurrency on Webull. However, we do not support transferring crypto into or out of your Webull account at this time.

