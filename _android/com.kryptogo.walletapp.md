---
wsId: kryptogo
title: KryptoGO -Bitcoin & NFT Wallet
altTitle: 
authors:
- danny
users: 5000
appId: com.kryptogo.walletapp
appCountry: 
released: 2021-12-28
updated: 2024-05-16
version: 2.36.0
stars: 4.6
ratings: 
reviews: 3
size: 
website: https://kryptogo.com
repository: 
issue: 
icon: com.kryptogo.walletapp.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-04-28
signer: 
reviewArchive: 
twitter: kryptogo_
social:
- https://www.facebook.com/kryptogo
- https://www.linkedin.com/company/kryptogo/
- https://www.instagram.com/kryptogo_/
- https://t.me/kryptogocom
- https://www.youtube.com/channel/UCrj-kZUkRde6tvin7DKhfQQ
redirect_from: 
developerName: KryptoGO
features: 

---

**Note:** The app has three related domains on its developer contacts. 

- stickey.app
- stickeygo.com
- kryptogo.com

## App Description from Google Play 

> KryptoGO Wallet is a non-custodial wallet, which allows you and nobody else full control over your assets.

## Analysis 

We downloaded and [installed the app](https://twitter.com/BitcoinWalletz/status/1651859617209810944).

- Seed phrases were provided
- There is a Bitcoin wallet that can send and receive 
- We were not able to locate the specific repository for the Android app. 

We messaged them on twitter but for the meanwhile, we'll tag this as having **no publicly available source.**