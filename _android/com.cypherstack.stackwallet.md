---
wsId: stackWallet
title: Stack Wallet
altTitle: 
authors:
- danny
users: 5000
appId: com.cypherstack.stackwallet
appCountry: 
released: 2024-05-13
updated: 2024-05-13
version: 2.0.0
stars: 
ratings: 
reviews: 
size: 
website: 
repository: https://github.com/cypherstack/stack_wallet/tags
issue: https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/503
icon: com.cypherstack.stackwallet.png
bugbounty: 
meta: ok
verdict: wip
date: 2023-09-08
signer: 
reviewArchive: 
twitter: stack_wallet
social:
- https://discord.com/invite/mRPZuXx3At
- https://t.me/stackwallet
- https://www.reddit.com/r/stackwallet
- https://www.youtube.com/channel/UCqCtpXsLyNIle1uOO2DU7JA
redirect_from: 
developerName: Cypher Stack Team
features: 

---

## App Description from Google Play

> Stack Wallet is a fully open source cryptocurrency wallet. With an easy to use user interface and quick and speedy transactions, this wallet is ideal for anyone no matter how much they know about the cryptocurrency space. The app is actively maintained to provide new user friendly features.
>
> Highlights include:
> - 10 Different cryptocurrencies
> - All private keys and seeds stay on device and are never shared.
> - Easy backup and restore feature to save all the information that's important to you.
> - Trading cryptocurrencies through our partners.
> - Custom address book
> - Favorite wallets with fast syncing
> - Custom Nodes.
> - Open source software.

## Analysis 

- We were provided with the seed phrases during wallet creation.
- Multiple wallets are available, and the BTC wallet addresses can be re-generated.
- They are Open Source, with an active GitHub repository.
- This app is slated **[for verification](https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/503)**.