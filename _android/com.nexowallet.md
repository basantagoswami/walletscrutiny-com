---
wsId: nexo
title: 'Nexo: Buy Bitcoin & Crypto'
altTitle: 
authors:
- leo
users: 1000000
appId: com.nexowallet
appCountry: 
released: 2019-06-28
updated: 2024-05-16
version: 4.4.1
stars: 4.1
ratings: 21149
reviews: 1803
size: 
website: https://nexo.com
repository: 
issue: 
icon: com.nexowallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-11-17
signer: 
reviewArchive: 
twitter: NexoFinance
social:
- https://www.facebook.com/nexofinance
- https://www.reddit.com/r/Nexo
redirect_from:
- /com.nexowallet/
developerName: Nexo Capital Inc
features: 

---

{% include review/bitgo.md %}

In the description on Google Play we read:

> • 100% Secured by Leading Audited Custodian BitGo

which makes it a custodial app. The custodian is claimed to be "BitGo" so as a
user you have to trust BitGo to not lose the coins and Nexo to actually not hold
all or part of the coins. In any case this app is **not verifiable**.
