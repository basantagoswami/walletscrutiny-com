---
wsId: arcticCryptoWallet
title: 'Arctic: Crypto Bitcoin Wallet'
altTitle: 
authors:
- danny
users: 10000
appId: com.arcticmobile.app
appCountry: 
released: 2022-12-15
updated: 2024-05-14
version: '1.33'
stars: 4.2
ratings: 
reviews: 1
size: 
website: https://arcticwallet.io
repository: 
issue: 
icon: com.arcticmobile.app.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-03
signer: 
reviewArchive: 
twitter: arctic_wallet
social:
- https://t.me/arctic_official_chat
- https://www.linkedin.com/company/arctic-wallet
- https://www.facebook.com/arcticwallet
- https://medium.com/@marketing_43986
redirect_from: 
developerName: ARCTIC SOFTWARE OÜ
features: 

---

## App Description from Google Play

> Arctic Wallet is a non-custodial crypto wallet built on the principles of decentralization and privacy.
With Arctic Wallet, you can send, receive and store your crypto safely and securely. Non-custodial means that your private keys are stored only on your device and nowhere else, only you have access to them.
>
> Buy Bitcoin (BTC), Ethereum (ETH), Ripple (XRP), Tether (USDT), Solana, and other crypto assets in minutes effortlessly for USD and EUR with your bank card right in your wallet.

## Analysis

- We started with a 4-digit passcode
- We were also given the option to use Biometrics.
- We then agreed with the Privacy Policy and Terms
- We were then asked to backup the 12-word seed phrase.
- There is a BTC legacy wallet address that has the following options:
  - Send
  - Swap
  - Buy
  - Receive
- There are no claims that the app is source-available. There were [0 results](https://github.com/search?q=com.arcticmobile.app&type=code) on GitHub when we did a code search using the app ID.
- This app is **not source-available**.
