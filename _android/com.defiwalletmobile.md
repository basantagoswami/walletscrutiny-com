---
wsId: slaviWallet
title: 'Slavi: DeFi Crypto Wallet'
altTitle: 
authors:
- danny
users: 50000
appId: com.defiwalletmobile
appCountry: 
released: 2021-12-13
updated: 2023-11-07
version: 1.25.2
stars: 4.8
ratings: 
reviews: 143
size: 
website: https://slavi.io/
repository: https://github.com/SlvLabs/slavi-wallet
issue: https://github.com/SlvLabs/slavi-wallet
icon: com.defiwalletmobile.png
bugbounty: 
meta: ok
verdict: nonverifiable
date: 2024-01-23
signer: 
reviewArchive: 
twitter: slavi_io
social:
- https://t.me/SlaviDappGroup
- https://www.youtube.com/channel/UCDkImGtFxBPMUBrAik0Ka9w
- https://medium.com/@SlaviDapp
- https://discord.com/invite/cJMYhXqRhD
- https://www.instagram.com/slavi.io
redirect_from: 
developerName: Slavi Development
features: 

---

## App Description from Google Play

> SLAVI is an all-in-one Wallet and platform with built-in cross-chain and Layer 2 solutions.
>
> It’s a new generation DeFi mobile wallet that supports 5000+ cryptocurrencies and 30+ integrated blockchains such as Binance Smart Chain, Polkadot, Polygon (Matic), Ethereum, TRON, SOLANA, AVAX, NEAR, METIS, MINA.
>
> The December Wallet version will give you the opportunity to store, send and receive tokens using multi-chain solutions based on BSC, BTC, ETH, Polygon, Litecoin, Doge, Polkadot. Also it includes cold wallet features for better protection.

## Analysis

- The first action the app needed us to take was to accept the User Agreement
- We were then asked to provide a 4-digit pin code.
- The app then generated a 12-word mnemonic
- The app generated a legacy BTC address that can send and receive.
- The providers did not claim the source was available but had a GitHub link to their organization.
- We were able to find a [repository](https://github.com/SlvLabs/slavi-wallet) for the app but it did not have releases/tags.

**Update 2024-01-09**

We tried reaching out to the [developers on GitHub](https://github.com/SlvLabs/slavi-wallet/issues/6), but they have not responded. 

The build instructions were very sparse, resulting in a lot of guess work on how the dockerfile was built. This did not result in a completed build. With no tags and releases, there is no way this app can be reproduced. This app is **non-verifiable**.
