---
wsId: Totalcoin
title: Totalcoin - Buy & Sell Bitcoin
altTitle: 
authors:
- leo
users: 500000
appId: io.totalcoin.wallet
appCountry: 
released: 2018-04-01
updated: 2024-03-19
version: 5.6.7.1877
stars: 3.7
ratings: 7981
reviews: 66
size: 
website: http://totalcoin.io
repository: 
issue: 
icon: io.totalcoin.wallet.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-05-24
signer: 
reviewArchive: 
twitter: 
social:
- https://www.facebook.com/totalcoin.io
redirect_from:
- /totalcoin/
- /io.totalcoin.wallet/
- /posts/2019/11/totalcoin/
- /posts/io.totalcoin.wallet/
developerName: TOTALCOIN TRADING LTD
features: 

---

On the wallet's website there is no claim about custodianship which makes us
assume it is a custodial product.

As such it is **not verifiable**.
