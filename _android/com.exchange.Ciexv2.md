---
wsId: ciexExchange
title: CIEx
altTitle: 
authors:
- danny
users: 5000
appId: com.exchange.Ciexv2
appCountry: 
released: 2022-07-20
updated: 2022-07-20
version: 5.4.0_push_v2
stars: 3.7
ratings: 
reviews: 
size: 
website: https://centurioninvest.com/
repository: 
issue: 
icon: com.exchange.Ciexv2.png
bugbounty: 
meta: stale
verdict: custodial
date: 2023-08-28
signer: 
reviewArchive: 
twitter: CENTURION_INVST
social:
- https://t.me/centurioninvestgroup
- https://www.youtube.com/channel/UCkjUQQCjZ5ed366GAUyRuog
- https://www.facebook.com/CenturionInvest
- https://www.linkedin.com/company/centurioninvest
- https://www.instagram.com/centurioninvest
redirect_from: 
developerName: Centurion Exchange
features: 

---

## App Description from Google Play

> CIEX provides the lowest fees on the cryptocurrency exchange market and best crypto prices from top leading liquidity providers.
>
> Trading with CIEX Exchange will be 100% secure - (WAF) detects and blocks hacking attempts, allowing our users to trade risk-free - your trading is reliably protected from fraud.

## Analysis 

- This exchange supports a multi-currency wallet including BTC.
- The exchange claims in its [terms](https://centurioninvest.com/termsconditions) that it maintains full custody of the digital assets.

This provider is explicit in its claims and thus the app is **non-verifiable**.