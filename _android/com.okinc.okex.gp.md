---
wsId: OKEx
title: 'OKX: Buy Bitcoin BTC & Crypto'
altTitle: 
authors:
- leo
users: 10000000
appId: com.okinc.okex.gp
appCountry: 
released: 2019-10-29
updated: 2024-05-18
version: 6.67.0
stars: 4.4
ratings: 188391
reviews: 1946
size: 
website: https://www.okx.com/
repository: 
issue: 
icon: com.okinc.okex.gp.png
bugbounty: 
meta: ok
verdict: custodial
date: 2020-05-29
signer: 
reviewArchive: 
twitter: OKEx
social:
- https://www.facebook.com/okexofficial
- https://www.reddit.com/r/OKEx
redirect_from:
- /com.okinc.okex.gp/
- /posts/com.okinc.okex.gp/
developerName: OKX
features: 

---

This app gives you access to a trading platform which sounds fully custodial and
therefore **not verifiable**.
