---
wsId: coreCryptoWallet
title: Core | Crypto Wallet & NFTs
altTitle: 
authors:
- danny
users: 100000
appId: com.avaxwallet
appCountry: 
released: 2022-08-30
updated: 2024-05-18
version: 0.14.9
stars: 3.8
ratings: 
reviews: 66
size: 
website: http://core.app
repository: 
issue: 
icon: com.avaxwallet.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-02
signer: 
reviewArchive: 
twitter: coreapp
social:
- https://t.me/avalancheavax
- https://discord.com/invite/RwXY7P6
- https://www.youtube.com/avalancheavax
- https://medium.com/@coreapp
- https://www.reddit.com/r/Avax
- https://www.facebook.com/corewallet
redirect_from: 
developerName: Ava Labs, Inc.
features: 

---

## App Description from Google Play

> ALL AT YOUR FINGERTIPS
- Self custody your assets so that you are the sole owner and not dependent on anyone else
- Keep track of your asset price movements in real-time with the Watchlist
- Buy AVAX quickly with a debit or credit card
- Easily swap Avalanche, Bitcoin, and Ethereum assets
- Manage your Avalanche and Ethereum assets, Bitcoin, NFTs, and more
- Transfer Bitcoin and Ethereum to participate in the DeFi ecosystem

## Analysis

- [(Screenshots)](https://twitter.com/BitcoinWalletz/status/1675311756405800960)
- We were given a 24-word mnemonic phrase during startup
- There was a bitcoin wallet with deposit and withdraw functionalities.
- The app provider did not make any claims as to the source-availability of the product.
- The results on GitHub for code search yielded 3 [unrelated instances.](https://github.com/search?q=com.avaxwallet&type=code)
- This app is **not source-available.**
