---
wsId: 
title: NGRAVE LIQUID - Crypto app
altTitle: 
authors:
- danny
users: 1000
appId: io.ngrave.liquid
appCountry: 
released: 2021-11-06
updated: 2024-04-09
version: 2.6.3
stars: 3.3
ratings: 
reviews: 12
size: 
website: https://www.ngrave.io/
repository: 
issue: 
icon: io.ngrave.liquid.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-06-01
signer: 
reviewArchive: 
twitter: ngrave_official
social:
- https://www.facebook.com/ngrave.io
- https://www.instagram.com/ngrave.io
- https://www.linkedin.com/company/ngrave
- https://discord.com/invite/gapxmWEBNJ
redirect_from: 
developerName: NGRAVEIO
features: 

---

## App Description from Google Play 

> Sync it with The Coldest Wallet NGRAVE ZERO by scanning QR codes without ever exposing your private keys. Sign your Crypto transactions offline and enjoy fast mobile transactions with maximum security.

## Analysis 

This is the companion app to the {% include walletLink.html wallet='hardware/ngravezero' verdict='true' %}. 

Its features cannot be accessed without pairing it to the device.

This is **not a wallet**.