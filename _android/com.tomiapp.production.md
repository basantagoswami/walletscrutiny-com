---
wsId: tomiPay
title: tomiPAY
altTitle: 
authors:
- danny
users: 50000
appId: com.tomiapp.production
appCountry: 
released: 2022-09-08
updated: 2024-05-23
version: '99.0'
stars: 
ratings: 
reviews: 
size: 
website: https://tomi.com
repository: 
issue: 
icon: com.tomiapp.production.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-19
signer: 
reviewArchive: 
twitter: tomipioneers
social:
- https://discord.com/invite/tomi
- https://www.reddit.com/r/tomipioneers
- https://t.me/tomipioneers
- https://www.tiktok.com/@tominetwork
- https://medium.com/tomipioneers
redirect_from: 
developerName: tomi.com
features: 

---

## App Description from Google Play

> Experience the ultimate self-custody solution with tomiPAY, a digital payment system with built-in privacy. With our app, you can safely and privately transact various currencies, Swap cryptocurrencies, and conveniently stake tomi tokens.
>
> Multichain Support: Unlock the potential of multiple blockchains with tomiPAY. Enjoy the freedom to transact across various chains, expanding your options and enabling greater financial flexibility.

## Analysis

- We were given a 15-word seed phrase, the app had a copy function.
- We confirmed it and later on found a Bech32 BTC address.
- The option to back up the private keys is in the settings.
- Another option in the app is 'staking'.
- We did not find any claims regarding source-availability and there was no link on their website to GitHub. 
- A code search on GitHub, had [0 results](https://github.com/search?q=com.tomiapp.production&type=repositories).
- This app is **not source-available.**