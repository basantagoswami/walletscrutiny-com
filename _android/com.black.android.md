---
wsId: 
title: black.com - Email, but better.
altTitle: 
authors:
- danny
users: 10000
appId: com.black.android
appCountry: 
released: 2021-07-09
updated: 2024-05-23
version: '49'
stars: 
ratings: 
reviews: 
size: 
website: https://black.com
repository: 
issue: 
icon: com.black.android.png
bugbounty: 
meta: ok
verdict: nowallet
date: 2023-07-17
signer: 
reviewArchive: 
twitter: 
social: 
redirect_from: 
developerName: black.com GmbH
features: 

---

## App Description from Google Play

> We are thrilled to introduce you to black.com - a brand-new premium email provider!
>
> Prestigious @black.com email address. Your perfect @black.com email address is probably still available.

## Analysis

- This is an email app with no reference to 'btc', 'bitcoin' or 'crypto'. After registration, they immediately asked for a fee.
- This app does **not have a wallet**.
