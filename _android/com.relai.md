---
wsId: relaiBuyBitcoin
title: 'Relai: Buy Bitcoin Easily'
altTitle: 
authors:
- danny
users: 100000
appId: com.relai
appCountry: 
released: 
updated: 2024-05-22
version: 2.8.7
stars: 
ratings: 
reviews: 
size: 
website: https://relai.app
repository: 
issue: 
icon: com.relai.png
bugbounty: 
meta: ok
verdict: nosource
date: 2023-07-11
signer: 
reviewArchive: 
twitter: Relai_app
social:
- https://www.linkedin.com/company/relai-app
- https://www.youtube.com/channel/UCBtN1U9Aa7KgQeS_gJicUBw
- https://t.me/relai_en
- https://www.instagram.com/relai.app
redirect_from: 
developerName: Relai
features: 

---

## App Description from Google Play

> You can buy Bitcoin in just a few clicks using your bank account, credit card, debit card, Apple Pay, or Google Pay.
>
> Relai's non-custodial wallet gives you full control of your Bitcoin. Safely store your 12-word recovery phrase, and recover your wallet anytime, anywhere.

## Analysis

- The provider claims to have a non-custodial wallet that supports Bitcoin.
- We searched on GitHub Code for the app ID, and found [8 results](https://github.com/search?q=com.relai&type=code) that are unrelated to the Android app.
- This app is **not source-available**.
