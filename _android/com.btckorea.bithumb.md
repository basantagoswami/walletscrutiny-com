---
wsId: bithumbko
title: Bithumb
altTitle: 
authors:
- leo
users: 5000000
appId: com.btckorea.bithumb
appCountry: 
released: 2017-09-26
updated: 2024-05-07
version: 3.2.0
stars: 3
ratings: 25850
reviews: 65
size: 
website: http://www.bithumb.com/
repository: 
issue: 
icon: com.btckorea.bithumb.png
bugbounty: 
meta: ok
verdict: custodial
date: 2021-02-19
signer: 
reviewArchive: 
twitter: BithumbOfficial
social:
- https://www.facebook.com/bithumb
redirect_from: 
developerName: Bithumb Korea
features: 

---

This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
