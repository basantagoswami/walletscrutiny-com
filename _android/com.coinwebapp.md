---
wsId: coinWebApp
title: Coinweb Wallet
altTitle: 
authors:
- danny
users: 1000
appId: com.coinwebapp
appCountry: 
released: 2023-04-28
updated: 2024-05-22
version: 1.0.31
stars: 
ratings: 
reviews: 
size: 
website: https://coinweb.io
repository: 
issue: 
icon: com.coinwebapp.png
bugbounty: 
meta: ok
verdict: nosource
date: 2024-04-10
signer: 
reviewArchive: 
twitter: CoinwebOfficial
social:
- http://t.me/coinweb
- https://discord.com/invite/cWSQD3wJqY
redirect_from: 
developerName: Coinweb
features: 

---

## App Description 

The app is an accounts based, email registration required multi-coin wallet. It supports BTC as well as other coins.

## Analysis 

- The seed phrases were provided after email verification.
- There is a BTC wallet
- We did not find any links to a repository making this app **not source-available.**

