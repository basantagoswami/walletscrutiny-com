---
title: Trezor-Tropic01 Open Secure Element Chip
appId: trezor.tropic01
authors:
- danny
released: 
discontinued: 
updated: 
version: 
binaries: 
dimensions:
weight: 
provider: TropicSquare
providerWebsite: 
website: https://tropicsquare.com/
shop: https://tropicsquare.com/product
country: CZ
price: 
repository: 
issue: 
icon: trezor.tropic01.png
bugbounty: 
meta: ok
verdict: unreleased
date: 2023-12-06
signer: 
reviewArchive: 
twitter: tropicsquare
social:
features: 

---

The product specification sheets could be found [here.](https://assets-global.website-files.com/625faf6f5e93e941317bb67f/64e47c10b8fd2d6c9b22ed03_TROPIC01_DataBrief_Flyer.pdf)

This product is still in development.